
function videoFinishedCallback()
    -- This function is called when the video finishes playing
    -- You can add any additional logic here, like resetting the video
    print("Video finished playing")
end

BossMessageNumber=0
BossTimer=0

function PlayBossAproachDialogs(dt)
--print(MessageNumber)
--print(BossTimer)
BossTimer=BossTimer+dt
	if BossTimer>0 and BossTimer<5 and BossMessageNumber==0 then
		msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				
				--face = loadAsset("data/gfx/faces/pilot1.webp", "Image"),
				--face = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/data/gfx/faces/pilot1.webp"))),
				name = "Captain Zara Khan",
				--text = "Pilot, Enemy vessel incoming. Prepare for battle.",
				text = trans.bossText1,
				duration = 3,
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/BossDialogs/1Pilot_Enemy_vessel_incoming.ogg" )
	faceVideo:play()
	
		faceAudio = love.audio.newSource( "data/dubs/English/Boss/1Pilot_Enemy_vessel_incoming.ogg" ,"stream" )
		faceAudio:play()
		BossMessageNumber=1
	elseif BossTimer>5 and BossTimer <10 and BossMessageNumber==1 then
		msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				
				name = "Pilot Johnson",
				--text = "Roger that, Captain. I'm ready",
				text = trans.bossText2,
				duration = 2,
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/BossDialogs/2Rogerthat.ogg" )
	faceVideo:play()
	
		faceAudio = love.audio.newSource( "data/dubs/English/Boss/2Rogerthat.ogg" ,"stream" )
		faceAudio:play()
		BossMessageNumber=2
	elseif BossTimer>10 and BossTimer <22 and BossMessageNumber==2 then
		msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				
				name = "Captain Zara Khan",
				--text = "Be advised, Pilot. This isn't going to be easy. Their ship is heavily armed and well-protected. You must be cautious if you want to come out victorious.",
				text = trans.bossText3,
				duration = 12,
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/BossDialogs/3Be_advised_Pilot.ogg" )
	faceVideo:play()
	
		faceAudio = love.audio.newSource( "data/dubs/English/Boss/3Be_advised_Pilot.ogg" ,"stream" )
		faceAudio:play()
		BossMessageNumber=3
		
		elseif BossTimer>23 and BossTimer <26 and BossMessageNumber==3 then
		
			msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				
				name = "Pilot Johnson",
				--text = "Got it, Captain",
				text = trans.bossText4,
				duration = 3,
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/BossDialogs/4GotIt.ogg" )
	faceVideo:play()
	
		faceAudio = love.audio.newSource( "data/dubs/English/Boss/4GotIt.ogg" ,"stream" )
		faceAudio:play()
		BossMessageNumber=4
		
		elseif BossTimer>26 and BossTimer <29 and BossMessageNumber==4 then
		
			msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				
				name = "Captain Zara Khan",
				--text = "Good luck, Pilot. We're counting on you.",
				text = trans.bossText5,
				duration = 4,
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/BossDialogs/5GoodLuck.ogg" )
	faceVideo:play()
	
		faceAudio = love.audio.newSource( "data/dubs/English/Boss/5GoodLuck.ogg" ,"stream" )
		faceAudio:play()
		BossMessageNumber=5
	end
end

function PlaySomeDialogs(dt)
	timer=timer+dt

	if timer>0 and timer <4 and MessageNumber==1 then
	
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				
				--face = loadAsset("data/gfx/faces/pilot1.webp", "Image"),
				--face = love.graphics.newImage(WebP.loadImage(love.filesystem.newFileData("/data/gfx/faces/pilot1.webp"))),
				name = "Captain Zara Khan",
				--text = "This is Captain Zara Khan to Pilot Johnson. Come in, please.",
				text = trans.text1,
				duration = 5,
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/1_This_is_Captain_Zara_Khan_to_Pilot_Johnson_Come_in_please.ogg" )
	faceVideo:play()
	
		if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/1_This_is_Captain_Zara_Khan_to_Pilot_Johnson_Come_in_please.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/English/1_This_is_Captain_Zara_Khan_to_Pilot_Johnson_Come_in_please.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/1_This_is_Captain_Zara_Khan_to_Pilot_Johnson_Come_in_please.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/1_This_is_Captain_Zara_Khan_to_Pilot_Johnson_Come_in_please.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/1_This_is_Captain_Zara_Khan_to_Pilot_Johnson_Come_in_please.ogg" ,"stream" )
	end
		
	faceAudio:play()
	MessageNumber=2
	elseif timer>7 and timer <10 and MessageNumber==2 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot2.png"),
				--face = loadAsset("data/gfx/faces/pilot2.webp", "Image"),
				name = "Pilot Johnson",
				--text = "Johnston here, Commander. What's the situation?",
				text = trans.text2,
				duration = 5
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/2_Johnston_here_Commander_What_s_the_situation.ogg" )
	faceVideo:play()
	
		if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/2_Johnston_here_Commander_What_s_the_situation.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/Spanish/2_Johnston_here_Commander_What_s_the_situation.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/2_Johnston_here_Commander_What_s_the_situation.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/2_Johnston_here_Commander_What_s_the_situation.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/2_Johnston_here_Commander_What_s_the_situation.ogg" ,"stream" )
	end
	faceAudio:play()
	
	MessageNumber=3
	
	elseif timer>10 and timer <18 and MessageNumber==3 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				name = "Captain Zara Khan",
				--text = "We've detected a strange energy signature coming from the nearby planetary system.",
				text = trans.text3,
				duration = 5
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/3_We_ve_detected_a_strange_energy_signature_coming_from_the_nearby_planetary_system.ogg" )
	faceVideo:play()
	
		if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/3_We_ve_detected_a_strange_energy_signature_coming_from_the_nearby_planetary_system.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/English/3_We_ve_detected_a_strange_energy_signature_coming_from_the_nearby_planetary_system.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/3_We_ve_detected_a_strange_energy_signature_coming_from_the_nearby_planetary_system.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/3_We_ve_detected_a_strange_energy_signature_coming_from_the_nearby_planetary_system.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/3_We_ve_detected_a_strange_energy_signature_coming_from_the_nearby_planetary_system.ogg" ,"stream" )
	end
	faceAudio:play()
	
	MessageNumber=4
	
	elseif timer>18 and timer <23 and MessageNumber==4 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot2.png"),
				name = "Pilot Johnson",
				--text = "Roger that. ETA to the planet surface is...3 minutes.",
				text = trans.text4,
				duration = 5
			}
	})
	
	faceVideo = love.graphics.newVideo( "data/videos/4_Roger_that_ETA_to_the_planet_surface_is_3_minutes.ogg" )
	faceVideo:play()
	
	if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/4_Roger_that_ETA_to_the_planet_surface_is_3_minutes.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/Spanish/4_Roger_that_ETA_to_the_planet_surface_is_3_minutes.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/4_Roger_that_ETA_to_the_planet_surface_is_3_minutes.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/4_Roger_that_ETA_to_the_planet_surface_is_3_minutes.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/4_Roger_that_ETA_to_the_planet_surface_is_3_minutes.ogg" ,"stream" )
	end
	faceAudio:play()
	
	MessageNumber=5
	
	elseif timer>23 and timer <25 and MessageNumber==5 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot2.png"),
				name = "Pilot Johnson",
				--text = "Starting descent now.",
				text = trans.text5,
				duration = 5
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/5_Starting_descent_now.ogg" )
	faceVideo:play()
	
	if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/5_Starting_descent_now.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/Spanish/5_Starting_descent_now.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/5_Starting_descent_now.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/5_Starting_descent_now.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/5_Starting_descent_now.ogg" ,"stream" )
	end
	faceAudio:play()
	
	
	MessageNumber=6
	
		
	elseif timer>24 and timer <35 and MessageNumber==6 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				name = "Captain Zara Khan",
				--text = "Negative, Pilot. We just picked up another signal - it's an alien vessel heading straight for the planet!",
				text = trans.text6,
				duration = 5
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/6_Negative_Pilot_We_just_picked_up-another_signal_it_s_an_alien_vessel_heading_straight_for_the_planet.ogg" )
	faceVideo:play()
	
	if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/6_Negative_Pilot_We_just_picked_up-another_signal_it_s_an_alien_vessel_heading_straight_for_the_planet.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/English/6_Negative_Pilot_We_just_picked_up-another_signal_it_s_an_alien_vessel_heading_straight_for_the_planet.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/6_Negative_Pilot_We_just_picked_up-another_signal_it_s_an_alien_vessel_heading_straight_for_the_planet.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/6_Negative_Pilot_We_just_picked_up-another_signal_it_s_an_alien_vessel_heading_straight_for_the_planet.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/6_Negative_Pilot_We_just_picked_up-another_signal_it_s_an_alien_vessel_heading_straight_for_the_planet.ogg" ,"stream" )
	end
	faceAudio:play()
	
	MessageNumber=7
	
		elseif timer>35 and timer <42 and MessageNumber==7 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot2.png"),
				name = "Pilot Johnson",
				--text = "What?! That changes things. Can you give me any information on the enemy craft?",
				text = trans.text7,
				duration = 5
			}
	})
	
	faceVideo = love.graphics.newVideo( "data/videos/7_What_That_changes_things_Can_you_give_me_any_information_on_the_enemy_craft.ogg" )
	faceVideo:play()
	
	if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/7_What_That_changes_things_Can_you_give_me_any_information_on_the_enemy_craft.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/Spanish/7_What_That_changes_things_Can_you_give_me_any_information_on_the_enemy_craft.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/7_What_That_changes_things_Can_you_give_me_any_information_on_the_enemy_craft.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/7_What_That_changes_things_Can_you_give_me_any_information_on_the_enemy_craft.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/7_What_That_changes_things_Can_you_give_me_any_information_on_the_enemy_craft.ogg" ,"stream" )
	end
	faceAudio:play()
	
	
	MessageNumber=8
	
		elseif timer>42 and timer <50 and MessageNumber==8 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				name = "Captain Zara Khan",
				--text = "It's a heavy cruiser, heavily armed. They're closing fast. You need to act quickly.",
				text = trans.text8,
				duration = 5
			}
	})
	
	faceVideo = love.graphics.newVideo( "data/videos/8_It_s_a_heavy_cruiser_heavily_armed_They_re_closing_fast_You_need_to_act_quickly.ogg" )
	faceVideo:play()
	
	if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/8_It_s_a_heavy_cruiser_heavily_armed_They_re_closing_fast_You_need_to_act_quickly..ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/English/8_It_s_a_heavy_cruiser_heavily_armed_They_re_closing_fast_You_need_to_act_quickly..ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/8_It_s_a_heavy_cruiser_heavily_armed_They_re_closing_fast_You_need_to_act_quickly..ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/8_It_s_a_heavy_cruiser_heavily_armed_They_re_closing_fast_You_need_to_act_quickly..ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/8_It_s_a_heavy_cruiser_heavily_armed_They_re_closing_fast_You_need_to_act_quickly..ogg" ,"stream" )
	end
	faceAudio:play()
	
	MessageNumber=9
	
			elseif timer>50 and timer <55 and MessageNumber==9 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot2.png"),
				name = "Pilot Johnson",
				--text = "I'll take care of it. Out.",
				text = trans.text9,
				duration = 5
			}
	})
	
	faceVideo = love.graphics.newVideo( "data/videos/9_I_ll_take_care_of_it_Out.ogg" )
	faceVideo:play()
	
	if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/9_I_ll_take_care_of_it_Out.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/Spanish/9_I_ll_take_care_of_it_Out.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/9_I_ll_take_care_of_it_Out.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/9_I_ll_take_care_of_it_Out.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/9_I_ll_take_care_of_it_Out.ogg" ,"stream" )
	end
	faceAudio:play()
	
	MessageNumber=10
	
				elseif timer>55 and timer <70 and MessageNumber==10 then
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/pilot1.png"),
				name = "Captain Zara Khan",
				--text = "Good luck, Pilot. Hope you know what you're doing.",
				text = trans.text10,
				duration = 5
			}
	})
	faceVideo = love.graphics.newVideo( "data/videos/10_Good_luck_Pilot_Hope_you_know_what_you_re_doing.ogg" )
	faceVideo:play()
	
	if game.language=="en" then
			faceAudio = love.audio.newSource( "data/dubs/English/10_Good_luck_Pilot_Hope_you_know_what_you_re_doing.ogg" ,"stream" )
	elseif game.language=="es" then
			faceAudio = love.audio.newSource( "data/dubs/English/10_Good_luck_Pilot_Hope_you_know_what_you_re_doing.ogg" ,"stream" )
	elseif game.language=="de" then
		faceAudio = love.audio.newSource( "data/dubs/German/10_Good_luck_Pilot_Hope_you_know_what_you_re_doing.ogg" ,"stream" )
	elseif game.language=="fr" then
		faceAudio = love.audio.newSource( "data/dubs/French/10_Good_luck_Pilot_Hope_you_know_what_you_re_doing.ogg" ,"stream" )
	else
		faceAudio = love.audio.newSource( "data/dubs/English/10_Good_luck_Pilot_Hope_you_know_what_you_re_doing.ogg" ,"stream" )
	end
	faceAudio:play()
	
	MessageNumber=11
	love.graphics.setColor(1,1,1,1)
	sound.bgm:setVolume(1)
	end
end
