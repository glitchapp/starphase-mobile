function isMobile()
    local osString = love.system.getOS()

    if osString == "Linux" then
        -- Check the system architecture
        local arch = io.popen("uname -m"):read("*all")
        if arch:match("arm") or arch:match("aarch64") then
            return true
        end

        -- Check for Android-specific environment variables
        local envVars = {
            "ANDROID_ROOT",
            "ANDROID_DATA",
            "ANDROID_BOOTLOGO"
        }

        for _, var in ipairs(envVars) do
            if os.getenv(var) then
                return true
            end
        end

        -- Check for DMI data, which is usually present on desktop systems
        local dmi = io.popen("cat /sys/class/dmi/id/product_name 2>/dev/null"):read("*all")
        if dmi and dmi ~= "" then
            return false
        end

        -- Check the kernel command line for mobile-specific parameters
        local cmdline = io.popen("cat /proc/cmdline 2>/dev/null"):read("*all")
        if cmdline:match("android") or cmdline:match("samsung") or cmdline:match("huawei") then
            return true
        end

        -- As a last resort, check for the existence of typical mobile directories
        local mobilePaths = {
            "/system/build.prop", -- Android
            "/system/bin/bootanimation", -- Android
            "/data/data" -- Android
        }

        for _, path in ipairs(mobilePaths) do
            local file = io.open(path, "r")
            if file then
                file:close()
                return true
            end
        end
    end

    return false
end

function getOS()
    local osString = love.system.getOS()
    if osString == "Linux" then
        if isMobile() then
            return "Mobile Linux"
        else
            return "Desktop Linux"
        end
    end
    return osString
end

-- Usage example
--local currentOS = getOS()
--print("Current OS: " .. currentOS)
