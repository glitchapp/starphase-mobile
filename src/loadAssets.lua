-- Table to store loaded assets
loadedAssets = {}

-- Function to load an asset if not already loaded
function loadAsset(assetPath, assetType)
    if not loadedAssets[assetPath] then
        if assetType == "Image" then
            local webpData = love.filesystem.newFileData(assetPath)
            loadedAssets[assetPath] = love.graphics.newImage(WebP.loadImage(webpData))
        -- Add more conditions for other asset types if needed
        end
    end
    return loadedAssets[assetPath]
end


-- Function to load an asset if not already loaded
function loadAssetImage(assetPath)
    if not loadedAssets[assetPath] then
            local webpData = love.filesystem.newFileData(assetPath)
            loadedAssets[assetPath] = love.graphics.newImage(WebP.loadImage(webpData))
    end
    return loadedAssets[assetPath]
end
