--[[
 * Copyright (C) 2016-2019 Ricky K. Thomson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * u should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 --]]

title = {}
title.splash = true
title.splash_logo = love.graphics.newImage("data/gfx/artsoftware.png")
title.splashDelay = 1
title.splashCycle = 1
title.splashOpacity = 1
title.splashOpacityFadeSpeed = 0.4


title.active = "main"
title.menu = {}
title.menu.w = 600
title.menu.h = 400
title.menu.x = love.graphics.getWidth() - title.menu.w
title.menu.y = title.menu.h - title.menu.h/4
title.menu.canvas = love.graphics.newCanvas(title.menu.w,title.menu.h)
title.menu.selected = 1
title.menu.maxoptions = 0

title.opacity = 1
title.opacitystep = 1
title.opacitymin = 0.4
title.opacitymax = 1

title.overlay = {}
title.overlay.opacity = 0
title.overlay.fadeout = false
title.overlay.fadein = false
title.overlay.fadespeed = 0.58

title.scene_timer = 0
title.scene_delay = 5

title.sounds = {}
title.sounds.option = love.audio.newSource("data/sfx/menu/option.ogg","static")
title.sounds.select = love.audio.newSource("data/sfx/menu/select.ogg","static")
title.sounds.option:setVolume(0.75)
title.sounds.select:setVolume(0.75)
	
title.ship1 = love.graphics.newImage("data/gfx/player/1_large.png")
title.ship2 = love.graphics.newImage("data/gfx/player/2_large.png")
title.ship3 = love.graphics.newImage("data/gfx/player/3_large.png")


title.gradient = love.graphics.newImage("data/gfx/gradient_black.png")

require("Credits/Credits")
creditsload()



function title:init()
	starfield:setSeed()
	title.overlay.fadein = true
	title.overlay.opacity = 1
	title.menu.maxoptions = 6
	paused = false
	mode = "title"
	title.active = "main"
	love.mouse.setVisible(true)
	love.mouse.setGrabbed(true)
	starfield.offset = 0  
	starfield.speed = 0
	starfield.minspeed = 10
	starfield.maxspeed = 1000
	starfield:populate()
	love.graphics.setBackgroundColor(0,0,0,1)
	
	sound:playbgm(1)
end

function title:update(dt)
	if title.active == "credits" then
		updateCredits(dt)
	end
	if title.splash then
		if #load.files > 0 then
			load:update(dt)
			return
		end

		title.splashCycle = math.max(0, title.splashCycle - dt)
		
		if title.splashCycle <= 0 then
			if title.splashOpacity > 0 then
				title.splashOpacity = title.splashOpacity - title.splashOpacityFadeSpeed *dt
			else
				title.overlay.fadein = true
				title.splashCycle = title.splashDelay
				title.splash = false
			end
		end
	end

	title.opacity = (title.opacity - title.opacitystep*dt)
	if title.opacity < title.opacitymin  then
	title.opacity = title.opacitymin
		title.opacitystep = -title.opacitystep
	end
	if title.opacity > title.opacitymax  then
		title.opacity = title.opacitymax
		title.opacitystep = -title.opacitystep
	end		
	
	if title.overlay.fadeout then
		title.overlay.opacity = title.overlay.opacity +title.overlay.fadespeed *dt
		
		if title.overlay.opacity > 1 then
			title.overlay.opacity = 0
			title.overlay.fadeout = false
		end
	end
	
	if title.overlay.fadein then
		title.overlay.opacity = title.overlay.opacity -title.overlay.fadespeed *dt
		
		if title.overlay.opacity < 0 then
			title.overlay.opacity = 0
			title.overlay.fadein = false
		end
	end
	
	starfield:update(dt)

end

function title:draw()
	if title.splash then
		love.graphics.setColor(1,1,1,title.splashOpacity)
		love.graphics.draw(title.splash_logo,love.graphics.getWidth()/2-title.splash_logo:getWidth()/2, love.graphics.getHeight()/2-title.splash_logo:getHeight()/2)

		love.graphics.setFont(fonts.title_large)		
		love.graphics.setColor(1,1,1,title.splashOpacity)
		love.graphics.printf("Loading", 0,love.graphics.getHeight()/10 ,love.graphics.getWidth(),"center")

		if #load.files > 0 then
			load:draw()
		end

		return
	end
	
	starfield:draw(0,0)

	
	if debugstarfield then	
		starfield:drawPalette()
		return 
	end


	love.graphics.setColor(1,1,1,1)
	for i=0, love.graphics.getHeight(), 1 do
		love.graphics.draw(title.gradient, love.graphics.getWidth()-title.gradient:getWidth()/2,i,0,0.5,1)
	end
			
	love.graphics.setCanvas(title.menu.canvas)
	love.graphics.clear()
	

	love.graphics.setFont(fonts.title_large)		
	local wrap = 500
			
	--title
	love.graphics.setColor(1,1,1,0.9)
	love.graphics.printf("Star Phase", 0,0,wrap,"center",0,1,1)
	love.graphics.setColor(1,1,1,0.4)
	love.graphics.printf("Star Phase", 10,0,wrap,"center",0,1,1)

	--menu
	love.graphics.setFont(fonts.title_select)
		
	if title.active == "main" then
	self:itemselected(1)
		love.graphics.printf(trans.InfiniteMode, 300,100,wrap,"left",0,1,1)
		self:itemselected(2)
		love.graphics.printf(trans.TwoPlayerMode, 300,140,wrap,"left",0,1,1)
		self:itemselected(3)
		love.graphics.printf(trans.DebugMode, 300,180,wrap,"left",0,1,1)
		self:itemselected(4)
		love.graphics.printf(trans.Settings, 300,220,wrap,"left",0,1,1)
		self:itemselected(5)
		love.graphics.printf(trans.SpaceViewer, 300,260,wrap,"left",0,1,1)
		self:itemselected(6)
		love.graphics.printf(trans.Credits, 300,300,wrap,"left",0,1,1)
		self:itemselected(7)
		love.graphics.printf(trans.ExitToDesktop, 300,340,wrap,"left",0,1,1)
	elseif title.active == "settings" then
		self:itemselected(1)
		love.graphics.printf(trans.Video, 300,100,wrap,"left",0,1,1)
		self:itemselected(2)
		love.graphics.printf(trans.Sound, 300,140,wrap,"left",0,1,1)
		self:itemselected(3)
		love.graphics.printf(trans.Controls, 300,180,wrap,"left",0,1,1)
		self:itemselected(4)
		love.graphics.printf(trans.Network, 300,220,wrap,"left",0,1,1)
		self:itemselected(5)
		love.graphics.printf(trans.Languages, 300,260,wrap,"left",0,1,1)
		self:itemselected(6)
		love.graphics.printf(trans.Back, 300,300,wrap,"left",0,1,1)
	elseif title.active == "languages" then
		self:itemselected(1)
		love.graphics.printf(trans.Danish, 200,100,wrap,"left",0,1,1)
		self:itemselected(2)
		love.graphics.printf(trans.Dutch, 200,140,wrap,"left",0,1,1)
		self:itemselected(3)
		love.graphics.printf(trans.English, 200,180,wrap,"left",0,1,1)
		self:itemselected(4)
		love.graphics.printf(trans.Finnish, 200,220,wrap,"left",0,1,1)
		self:itemselected(5)
		love.graphics.printf(trans.French, 200,260,wrap,"left",0,1,1)
		self:itemselected(6)
		love.graphics.printf(trans.German, 200,300,wrap,"left",0,1,1)
		self:itemselected(7)
		love.graphics.printf(trans.Mandarin, 200,340,wrap,"left",0,1,1)
		self:itemselected(8)
		
		love.graphics.printf(trans.Norwegian, 400,100,wrap,"left",0,1,1)
		self:itemselected(9)
		love.graphics.printf(trans.Portuguese, 400,140,wrap,"left",0,1,1)
		self:itemselected(10)
		love.graphics.printf(trans.Spanish, 400,180,wrap,"left",0,1,1)
		self:itemselected(11)
		love.graphics.printf(trans.Swedish, 400,220,wrap,"left",0,1,1)
		self:itemselected(12)
		love.graphics.printf(trans.Back, 400,260,wrap,"left",0,1,1)
	elseif title.active == "video" then
		self:itemselected(1)
		love.graphics.printf(trans.Back, 300,100,wrap,"left",0,1,1)
	elseif title.active == "sound" then
		self:itemselected(1)
		love.graphics.printf(trans.Back, 300,100,wrap,"left",0,1,1)
	elseif title.active == "controls" then
		self:itemselected(1)
		love.graphics.printf(trans.Back, 300,100,wrap,"left",0,1,1)
	elseif title.active == "network" then
	love.graphics.printf("Network: ", 200,300,wrap,"left",0,1,1)
		if enableRemoteControls==true then love.graphics.printf("Enabled", 360,300,wrap,"left",0,1,1)
	elseif enableRemoteControls==false then  love.graphics.printf("Disabled", 360,300,wrap,"left",0,1,1)
	end
	
		self:itemselected(1)
		love.graphics.printf("enable network", 200,100,wrap,"left",0,1,1)
		self:itemselected(2)
		love.graphics.printf("disable network", 200,140,wrap,"left",0,1,1)
		self:itemselected(3)
		love.graphics.printf("set up network", 200,180,wrap,"left",0,1,1)
		self:itemselected(4)
		love.graphics.printf("back", 200,220,wrap,"left",0,1,1)
		self:itemselected(5)
	elseif title.active == "ship_selection" then
		self:itemselected(1)
		love.graphics.printf(trans.Ship1, 300,100,wrap,"left",0,1,1)
		self:itemselected(2)
		love.graphics.printf(trans.Ship2, 300,140,wrap,"left",0,1,1)
		self:itemselected(3)
		love.graphics.printf(trans.Ship3, 300,180,wrap,"left",0,1,1)
		self:itemselected(4)
		love.graphics.printf(trans.Back, 300,220,wrap,"left",0,1,1)
	end
	
	love.graphics.setColor(1,1,1,1)
	if title.active == "ship_selection" then
		if title.menu.selected == 1 then
			love.graphics.draw(title.ship3, 50,100, 0,0.5)
		elseif title.menu.selected == 2 then
			love.graphics.draw(title.ship1, 50,100, 0,0.5)
		elseif title.menu.selected == 3 then
			love.graphics.draw(title.ship2, 50,100, 0,0.5)
		end
	end
	
	love.graphics.setCanvas()
	
	if title.active == "credits" then
		creditsdraw(dt)
	end

	--border bars
	local h = 200
	love.graphics.setColor(0,0,0,1)
	love.graphics.rectangle("fill",0,love.graphics.getHeight()-h,love.graphics.getWidth(),h)
	love.graphics.rectangle("fill",0,0,love.graphics.getWidth(),h)
	
	love.graphics.setColor(0.3,0.3,0.3,0.5)
	love.graphics.line(0,love.graphics.getHeight()-h,love.graphics.getWidth(),love.graphics.getHeight()-h)
	love.graphics.line(0,h,love.graphics.getWidth(),h)
	
	love.graphics.setColor(1,1,1,1)
	love.graphics.draw(title.menu.canvas,title.menu.x,title.menu.y)
	
	
	love.graphics.setColor(1,1,1,0.4)
	love.graphics.setFont(fonts.default)
	local infostr = "v"..version..build.." ("..love.system.getOS() ..") by "..author
	love.graphics.printf(infostr,10,love.graphics.getHeight()-20,love.graphics.getWidth(),"center",0,1,1)		--version
	
	love.graphics.setColor(0,0,0,title.overlay.opacity)
	love.graphics.rectangle("fill",0,0,love.graphics.getWidth(),love.graphics.getHeight())
end


function title:navigate(d)
	sound:play(title.sounds.option)
	title.menu.selected = math.min(title.menu.maxoptions,math.max(1,title.menu.selected + d))
	title.opacity = 1 
	starfield:setSeed()
	starfield:populate()
end


function title:itemselected(selected)
	if title.menu.selected == selected then
		love.graphics.setColor(title.opacity*2,title.opacity*2,title.opacity*2,title.opacity)
	else
		love.graphics.setColor(0.5,0.5,0.5,0.5)
	end
end

leftxaxis,leftyaxis=0,0
function title:GamePadpressed(joystick,button)
	if #load.files > 0 then return end
	if debugstarfield then
		if (joystick and joystick:isGamepadDown("start")) or button2or6Pressed==true  then
		--if key == "escape" then 
			title.menu.selected = 4
			debugstarfield = false
			button2or6Pressed=false
			love.timer.sleep(0.1)
		--elseif key == "tab" then
		elseif (joystick and joystick:isGamepadDown("y")) or button3or7Pressed==true then
			starfield:setSeed()
			starfield:setColor()
			starfield:populate()		
			button3or7Pressed=false
			love.timer.sleep(0.1)
		end
		return
	end
	
	--if key == "escape" then 
	if (joystick and joystick:isGamepadDown("back")) or button1or5Pressed==true then
		button1or5Pressed=false
		love.event.quit() 		
		love.timer.sleep(0.1)
	end
	
	if title.splash then 
		--if key == "space" then 
		if (joystick and joystick:isGamepadDown("b")) or button0or4Pressed==true then
			title.splash = false 
			button0or4Pressed=false
			love.timer.sleep(0.1)
			return 
		end
	end
	
	--if key == "up" then 
	if leftyaxis<-0.2 or touchControls.leftyaxis<-0.2 then
		self:navigate(-1)
		love.timer.sleep(0.1)
	end
	--if key == "down" then 
	if leftyaxis>0.4 or touchControls.leftyaxis>0.2 then
		self:navigate(1)
		love.timer.sleep(0.1)
	end
		
	
	--if key == "return" then
	if (joystick and joystick:isGamepadDown("a")) or button2or6Pressed==true then

		sound:play(title.sounds.select)
				
		if title.active == "main" then
			if title.menu.selected == 1 then title.active = "ship_selection" title.menu.maxoptions = 4 end
			if title.menu.selected == 2 then title.active = "ship_selection" title.menu.maxoptions = 4 twoplayers=true end
			if title.menu.selected == 3 then initdebugarcade(3) end
			if title.menu.selected == 4 then title.active = "settings" title.menu.maxoptions = 6 end
			if title.menu.selected == 5 then debugstarfield = not debugstarfield end
			if title.menu.selected == 6 then --print ("unimplemented")
			title.active = "credits"
			 end
		elseif title.active == "settings" then
			if title.menu.selected == 1 then title.active = "video" title.menu.maxoptions = 1 end
			if title.menu.selected == 2 then title.active = "sound" title.menu.maxoptions = 1 end
			if title.menu.selected == 3 then title.active = "controls" title.menu.maxoptions = 1 end
			if title.menu.selected == 4 then title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 5 then title.active = "languages" title.menu.maxoptions = 12 end
			if title.menu.selected == 6 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "languages" then
			if title.menu.selected == 1 then title.active = "languages" game.language="da" TransDanish() title.menu.maxoptions = 12 end
			if title.menu.selected == 2 then title.active = "languages" game.language="du" TransDutch() title.menu.maxoptions = 12 end
			if title.menu.selected == 3 then title.active = "languages" game.language="en" TransEnglish() title.menu.maxoptions = 12 end
			if title.menu.selected == 4 then title.active = "languages" game.language="fi" TransFinnish() title.menu.maxoptions = 12 end
			if title.menu.selected == 5 then title.active = "languages" game.language="fr" TransFrench() title.menu.maxoptions = 12 end
			if title.menu.selected == 6 then title.active = "languages" game.language="de" TransGerman() title.menu.maxoptions = 12 end
			if title.menu.selected == 7 then title.active = "languages" game.language="ma" TransMandarin() title.menu.maxoptions = 12 end
			if title.menu.selected == 8 then title.active = "languages" game.language="no" TransNorwegian() title.menu.maxoptions = 12 end
			if title.menu.selected == 9 then title.active = "languages" game.language="po" TransPortuguese() title.menu.maxoptions = 12 end
			if title.menu.selected == 10 then title.active = "languages" game.language="es" TransSpanish() title.menu.maxoptions = 12 end
			if title.menu.selected == 11 then title.active = "languages" game.language="sw" TransSwedish() title.menu.maxoptions = 12 end
			if title.menu.selected == 12 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "video" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4  end
		elseif title.active == "sound" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4 end
		elseif title.active == "controls" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4 end
		elseif title.active == "network" then
			if title.menu.selected == 1 then title.active = "enable network" enableRemoteControls=true  title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 2 then title.active = "disable network" enableRemoteControls=false  title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 3 then title.active = "set up network" loadNetworkMenus() enableRemoteControls=true  menuOpened=true NetworkMenuVisible=true title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 4 then title.active = "settings" title.menu.maxoptions = 6 end
		elseif title.active == "ship_selection" then
			if title.menu.selected == 1 then initarcade(3) end
			if title.menu.selected == 2 then initarcade(1) end
			if title.menu.selected == 3 then initarcade(2) end
			if title.menu.selected == 4 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "credits" then
			if title.menu.selected == 1 then title.active = "main" title.menu.maxoptions = 7 end
		end
				
		title.menu.selected = 1
		button2or6Pressed=false
		love.timer.sleep(0.1)
	end
			
	if title.menu.selected < 1 then title.menu.selected = 1 end
	if title.menu.selected > title.menu.maxoptions then title.menu.selected = title.menu.maxoptions end
		
end

function title:keypressed(key)
	if #load.files > 0 then return end
	if debugstarfield then
		if key == "escape" then 
			title.menu.selected = 4
			debugstarfield = false
		elseif key == "tab" then
			starfield:setSeed()
			starfield:setColor()
			starfield:populate()		
		end
		return
	end
	
	if key == "escape" then 
		love.event.quit() 		
	end
	
	if title.splash then 
		if key == "space" then 
			title.splash = false 
			
			return 
		end
	end
	
	if key == "up" then 
		self:navigate(-1)
	end
	if key == "down" then 
		self:navigate(1)
	end
		
	
	if key == "return" then

		sound:play(title.sounds.select)
				
		if title.active == "main" then
			if title.menu.selected == 1 then title.active = "ship_selection" title.menu.maxoptions = 4 end
			if title.menu.selected == 2 then title.active = "ship_selection" title.menu.maxoptions = 4 twoplayers=true noobHubEnabled = true end
			if title.menu.selected == 3 then initdebugarcade(3) end
			if title.menu.selected == 4 then title.active = "settings" title.menu.maxoptions = 6 end
			if title.menu.selected == 5 then debugstarfield = not debugstarfield end
			if title.menu.selected == 6 then --print ("unimplemented")
			title.active = "credits"
			 end
		elseif title.active == "settings" then
			if title.menu.selected == 1 then title.active = "video" title.menu.maxoptions = 1 end
			if title.menu.selected == 2 then title.active = "sound" title.menu.maxoptions = 1 end
			if title.menu.selected == 3 then title.active = "controls" title.menu.maxoptions = 1 end
			if title.menu.selected == 4 then title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 5 then title.active = "languages" title.menu.maxoptions = 12 end
			if title.menu.selected == 6 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "languages" then
			if title.menu.selected == 1 then title.active = "languages" game.language="da" TransDanish() title.menu.maxoptions = 12 end
			if title.menu.selected == 2 then title.active = "languages" game.language="du" TransDutch() title.menu.maxoptions = 12 end
			if title.menu.selected == 3 then title.active = "languages" game.language="en" TransEnglish() title.menu.maxoptions = 12 end
			if title.menu.selected == 4 then title.active = "languages" game.language="fi" TransFinnish() title.menu.maxoptions = 12 end
			if title.menu.selected == 5 then title.active = "languages" game.language="fr" TransFrench() title.menu.maxoptions = 12 end
			if title.menu.selected == 6 then title.active = "languages" game.language="de" TransGerman() title.menu.maxoptions = 12 end
			if title.menu.selected == 7 then title.active = "languages" game.language="ma" TransMandarin() title.menu.maxoptions = 12 end
			if title.menu.selected == 8 then title.active = "languages" game.language="no" TransNorwegian() title.menu.maxoptions = 12 end
			if title.menu.selected == 9 then title.active = "languages" game.language="po" TransPortuguese() title.menu.maxoptions = 12 end
			if title.menu.selected == 10 then title.active = "languages" game.language="es" TransSpanish() title.menu.maxoptions = 12 end
			if title.menu.selected == 11 then title.active = "languages" game.language="sw" TransSwedish() title.menu.maxoptions = 12 end
			if title.menu.selected == 12 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "video" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4  end
		elseif title.active == "sound" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4 end
		elseif title.active == "controls" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4 end
		elseif title.active == "network" then
			if title.menu.selected == 1 then title.active = "enable network" enableRemoteControls=true  end
			if title.menu.selected == 2 then title.active = "disable network" enableRemoteControls=false  end
			if title.menu.selected == 3 then title.active = "set up network" loadNetworkMenus() enableRemoteControls=true  menuOpened=true NetworkMenuVisible=true title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 4 then title.active = "settings" title.menu.maxoptions = 6 end
		elseif title.active == "ship_selection" then
			if title.menu.selected == 1 then initarcade(3) end
			if title.menu.selected == 2 then initarcade(1) end
			if title.menu.selected == 3 then initarcade(2) end
			if title.menu.selected == 4 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "credits" then
			if title.menu.selected == 1 then title.active = "main" title.menu.maxoptions = 7 end
		end
				
		title.menu.selected = 1
	end
			
	if title.menu.selected < 1 then title.menu.selected = 1 end
	if title.menu.selected > title.menu.maxoptions then title.menu.selected = title.menu.maxoptions end
		
end
