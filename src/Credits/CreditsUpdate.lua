--The "creditsupdate" function updates the position of the credits text by decreasing the "credposition" variable by a factor of "credspeed" multiplied by "dt". This function is likely called within the "love.update" function to update the position of the credits text every frame.

function updateCredits(dt)
        
        credposition = credposition - (credspeed * dt)  -- x will increase by 32 for every second right is held down
        		
end


function updateColors()
local minCredPosition = -35000
local maxCredPosition = 800

-- Map credposition to a value between 0 and 1
local colorFactor = (credposition - minCredPosition) / (maxCredPosition - minCredPosition)

-- Limit the colorFactor to the range [0, 1]
colorFactor = math.min(1, math.max(0, colorFactor))

-- Use colorFactor to adjust the color
local red = 1 - colorFactor  -- Adjust as needed
local green = colorFactor    -- Adjust as needed
local blue = 0.5             -- You can set a constant value or adjust based on colorFactor

-- Set the color
love.graphics.setColor(red, green, blue)
end
