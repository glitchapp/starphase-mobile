local socket = require("socket.http") -- Import LuaSocket
local http = require("socket.http")  -- Explicitly load the http module from socket
local ltn12 = require("ltn12") -- Required for HTTP requests
json = require("chatHub/dkjson") -- Import dkjson
--local json = require("json") -- JSON library for encoding/decoding

local activeBackend = "gpt4all" -- Default to GPT4All

-- GPT4All API configuration
local GPT4ALL_API_URL = "http://localhost:4891/v1/chat/completions"
local GPT4ALL_MODEL = "Phi-3 Mini Instruct"
local GPT4ALL_TEMPERATURE = 0.28
local GPT4ALL_MAX_TOKENS = 50

local OLLAMA_API_URL = "http://localhost:11434/api/generate"

-- Chat HUD variables
--local messages = {} -- Stores chat messages
--local input_text = "" -- Stores user input
--local username = "Player" -- Default username
local scrollOffset = 0 -- Tracks how much the chat HUD has scrolled
local maxLines = 10 -- Maximum number of lines to display in the chat HUD

-- Add a state variable to track if GPT is thinking
local isThinking = false
--local audioIsBeingProcessed = false



local thread = love.thread.newThread("chatHub/gpt_thread.lua")
local TTS_thread = love.thread.newThread("chatHub/gpt_thread.lua")

-- Create channels for communication with the TTS worker thread
local channelInput = love.thread.getChannel("input_channel")
local channelOutput = love.thread.getChannel("output_channel")

thread:start()

local autocompleteKeywords = {
    "set temperature to",
    "set max tokens to",
    "set model to",
    "switch to gpt4all",
    "switch to ollama",
    "enable touch controls",
    "disable touch controls",
    "enable music",
    "disable music",
    "set zoom to",
    "set pixelsize to",
    "change language to",
    "next language",
    "enable speech",
    "disable speech",
    "change speaker to",
    "quit game",
    "send to channel",
    "send to user"
}

local function getAutocompleteSuggestions(input)
    local suggestions = {}
    local lowerInput = input:lower()
    
    for _, keyword in ipairs(autocompleteKeywords) do
        if keyword:lower():find(lowerInput, 1, true) == 1 then
            table.insert(suggestions, keyword)
        end
    end
    
    return suggestions
end

function sendToGPT4AllAsync(message)
    -- Adjust max_tokens and temperature based on the user input
    local maxTokens = GPT4ALL_MAX_TOKENS
    local temperature = GPT4ALL_TEMPERATURE
    local systemContent = ""  -- Define system content dynamically

    if message:match("brief") then
        maxTokens = 150  -- Lower token limit for brief responses
        temperature = 0.3  -- More deterministic response for brief queries
    elseif message:match("detailed") then
        maxTokens = 300  -- More tokens for detailed answers
        temperature = 0.7  -- More creative responses for detailed queries
    end
    
    -- Add dynamic system context based on the query or game state
    if message:match("mission") then
        systemContent = systemContent .. "\n### Mission Details: Your current mission is critical to the war effort. Pay attention to key objectives and obstacles."
        
         -- New: Game state-based context additions
    -- Shield status
    if counters then
    if counters.shields <= 2 then
        systemContent = systemContent .. string.format("\n### Shield Alert: CRITICAL (%d remaining). Use cover and prioritize defensive maneuvers.", counters.shields)
    end
	end
    
    if tgt_index then
    -- Orb placement progress
    if tgt_index <= 5 then
        systemContent = systemContent .. string.format("\n### Objective Update: %d/15 orbs remaining. Maintain focus on processor placement.", tgt_index)
    end
    end
	
    -- Time pressure (arcade mode only)
    if remainingTime and remainingTime < 60 then
        systemContent = systemContent .. string.format("\n### Time Warning: %d seconds remaining. Optimize route efficiency!", math.floor(remainingTime))
    end

        
    elseif message:match("enemy") then
        systemContent = systemContent .. "\n### Enemy Intel: You are facing Imperial androids. Knowledge of their weaknesses is essential for survival."
    
    elseif message:match("controls") then
        systemContent = systemContent .. "\n### Controls: Use the keyboard or gamepad for navigation and weapon usage. Refer to the manual for specific control mappings."
    elseif message:match("chemicals") then
        systemContent = systemContent .. "\n### Chemical Agents: Different androids require specific chemical agents. Collect and use them wisely."
    elseif message:match("switch") then
        systemContent = systemContent .. "\n### Switching Backend: Use commands like 'switch to gpt4all' or 'switch to ollama' to change backend models."
    end

    -- Parse the user input for local commands
    local commandResponse = parseUserInput(message)
    if commandResponse then
        table.insert(messages, { user = "System", text = commandResponse })
        return
    end

    -- Indicate that GPT is processing
    isThinking = true
    
    UIButton1Sound:play()		-- play an UI sound indicating that GPT is processing

    local requestBody, apiUrl

    if activeBackend == "gpt4all" then
        -- Construct the request body for GPT4All
        requestBody = {
            model = GPT4ALL_MODEL,
            max_tokens = maxTokens,
            temperature = temperature,
            messages = {
                {
                    role = "system",
                    content = systemContent .. "\n" .. [[
You are AI Agent C-7X "Vega," a Tactical Support AI. Your role is to assist players in tactical missions, providing precise instructions, updates, and menu guidance. Stay clear, concise, and efficient.

### Mission Background:
The war against the Galactic Empire continues. The Federal Resistance has captured Planet P2112, an artificial planet critical for the next offensive. The planet's atmosphere is incomplete, covered in red dust, and uninhabitable. Your mission is to collect 16 unstable orbs from underground and place them into atmospheric processors to purify the atmosphere. Be cautious—the orbs are unstable and will explode if not deposited in time.

### Enemy and Weapon Information:
Planet P2112 is guarded by Imperial androids. Each android type has a specific weakness to a chemical agent. Collect the correct chemical from boxes scattered across the planet. Chemicals have limited lifespans but can neutralize multiple androids of the same type.

### Gameplay Basics:
- **Objective**: Collect and place 16 unstable orbs into atmospheric processors.
- **Weapons**: Use the correct chemical agent for each android type. Chemicals are time-limited but reusable against multiple targets.

### Controls:
**Keyboard**:  
- **Menu**: W/S to navigate, P to select, O to go back, Number keys to select directly.  
- **Game**: P to thrust, O to release cable, I to use weapon/skip dialogs.

**Gamepad**:  
- **Menu**: Thumbstick to navigate, X to select, B to go back.  
- **Game**: A for thrust, Y to release cable, B to use weapon, Y to skip dialogs, Select to open launcher, Start to pause, Guide to open chat HUD.

Always provide clear directions and tactical advice during gameplay. Stay in character as Vega—neutral, logical, and efficient. Prioritize the player's success and enhance the gaming experience.
]]
                },
                {
                    role = "user",
                    content = message
                }
            }
        }
        apiUrl = GPT4ALL_API_URL
    else
        -- Construct the request body for Ollama
        requestBody = {
            model = "deepseek-r1",  -- Replace with the correct model name for Ollama
            prompt = message
        }
        apiUrl = OLLAMA_API_URL
    end

    -- Encode the request as JSON
    local requestBodyJson = json.encode(requestBody)
    if not requestBodyJson then
        print("Error encoding JSON")
        isThinking = false  -- Reset thinking state on error
        return
    end

    -- Send the request to the thread
    channelInput:push({
        url = apiUrl,
        body = requestBodyJson
    })
end


function checkThreadResponse()
     -- Check if there's a response from the thread
    local response = channelOutput:pop()
    if response then
        -- Reset thinking state once the response is received
        isThinking = false

        if response.success then
            -- Handle the successful response
            local gptResponse
            if activeBackend == "gpt4all" then
                gptResponse = response.response.choices[1].message.content
            else
                gptResponse = response.response.response -- Ollama's response structure
            end
            table.insert(messages, { user = activeBackend == "gpt4all" and "GPT4All" or "ollama", text = gptResponse })
            if speechEnabled == true then
				textToSpeech(gptResponse) -- Optionally play the response
			end
        else
            -- Handle errors
            table.insert(messages, { user = "System", text = "Error: " .. response.error })
        end
    end
end



                
       -- Function to monitor when the audio is done playing
        local function checkAudioDone()
			if source then
            if not source:isPlaying() then
                -- Call the callback when the audio finishes playing
                if callback then
                    callback()  -- Proceed to the next speech item in the queue
                end
            end
            end
        end

function chathudUpdate(dt)
    checkThreadResponse()
    
    if cooldownTimer <= 0 then
                checkAudioDone()  -- Check if the audio has finished
                cooldownTimer = 0.1  -- Reset the cooldown timer for the next check
    end
end



-- Save the original music volume before starting speech
--originalMusicVolume = music:getVolume()

-- Function to add text to the speech queue
function queueTextToSpeech(text)
    table.insert(speechQueue, text)
    if not isSpeaking then
        processSpeechQueue()  -- Start processing if no speech is in progress
    end
end

-- Function to process the speech queue
function processSpeechQueue()
    if #speechQueue > 0 then
        isSpeaking = true
        local text = table.remove(speechQueue, 1)
        textToSpeech(text, function()
            -- Callback when speech is done
            isSpeaking = false
            -- Add a cooldown before processing the next line (if necessary)
             
             -- Restore the music volume after speech finishes
            music:setVolume(originalMusicVolume)
            
            --cooldownTimer = cooldownDuration
            cooldownTimer = cooldownDuration
            -- Process the next item in the queue
            processSpeechQueue()
        end)
    end
end

        
function textToSpeech(text, callback)
    if not text or text == "" then
        print("Error: No text provided for speech synthesis.")
        return
    end

    print("Text being sent to TTS API:", text)
    
    --audioIsBeingProcessed = true	-- this flag will be used to print in the draw function that audio is being processed
      UIOnSound:play()	-- play an ui sound processing the audio

    -- URL encode the text for proper handling of special characters
    local encodedText = text:gsub(" ", "%%20")  -- Simple space encoding

    local url = "http://localhost:5002/api/tts?text=" .. encodedText
    -- Append optional parameters to the URL
    url = url .. "&speaker_id=default&language_id="

    -- Perform the HTTP GET request
    local responseBody = {}
    local res, code, headers, status = http.request {
        url = url,
        method = "GET",
        sink = ltn12.sink.table(responseBody)
    }

    print("Response Code:", code)
    print("Response Body:", table.concat(responseBody))

    if code == 200 then
       	--audioIsBeingProcessed = false
    
        local audioData = table.concat(responseBody)
        
        -- Save the audio data to a file (e.g., "output_audio.wav")
        local file = io.open("output_audio.wav", "wb")
        file:write(audioData)
        file:close()

        print("Audio data saved as output_audio.wav")
        
           -- Lower the music volume while speech is playing
        music:setVolume(0.1)  -- Set the music volume to a very low value (e.g., 0.1)
        
        -- Play the audio file
        local source = love.audio.newSource("output_audio.wav", "static")
        source:play()

	
        
    elseif code ~= 200 then
		--audioIsBeingProcessed = false
        print("Error: TTS server returned an error response.")
        return
    else
        print("Error generating speech:", code)
        print("Response Status:", status)

        -- Call the callback even if there's an error
        if callback then
            callback()
        else
            print("No callback provided.")
        end
    end
end




function parseUserInput(input)
    local lowerInput = input:lower()

    -- Check for temperature change command
    if lowerInput:match("set temperature to") then
        local newTemp = tonumber(lowerInput:match("set temperature to (%d+%.?%d*)"))
        if newTemp and newTemp >= 0 and newTemp <= 1 then
            GPT4ALL_TEMPERATURE = newTemp
            return "Temperature set to " .. newTemp
        else
            return "Invalid temperature value. Please provide a value between 0 and 1."
        end

    -- Check for max tokens change command
    elseif lowerInput:match("set max tokens to") then
        local newMaxTokens = tonumber(lowerInput:match("set max tokens to (%d+)"))
        if newMaxTokens and newMaxTokens > 0 then
            GPT4ALL_MAX_TOKENS = newMaxTokens
            return "Max tokens set to " .. newMaxTokens
        else
            return "Invalid max tokens value. Please provide a positive integer."
        end

    -- Check for model change command
    elseif lowerInput:match("set model to") then
        local newModel = lowerInput:match("set model to (.+)")
        if newModel then
            GPT4ALL_MODEL = newModel
            return "Model set to " .. newModel
        else
            return "Invalid model name. Please provide a valid model name."
        end
  
  elseif lowerInput:match("switch to gpt4all") then
        activeBackend = "gpt4all"
        return "Switched to GPT4All backend."
        
    elseif lowerInput:match("switch to ollama") then
        activeBackend = "ollama"
        return "Switched to Ollama backend."
        
 -- Check for touch controls toggle command
    elseif lowerInput:match("enable touch controls") then
        touchEnabled = true
        return "Touch controls enabled."

    elseif lowerInput:match("disable touch controls") then
        touchEnabled = false
        return "Touch controls disabled."

    -- Check for music toggle command
    elseif lowerInput:match("enable music") then
        if musicFlag == true then print("music already enabled")
        else
			toggleMusic()
			return "Music enabled."
		end

    elseif lowerInput:match("disable music") then
        if musicFlag == false then print("music already disabled")
        else
				toggleMusic()
                return "Music disabled."
		end
	 -- Check zoom change command
    elseif lowerInput:match("set zoom to") then
			main.pixelsize = tonumber(lowerInput:match("set zoom to (%d+)"))
        if main.pixelsize and main.pixelsize > 0 then
            return "Zoom set set to " .. main.pixelsize
        else
            return "Invalid zoom value. Please provide a positive integer."
        end	
        
	 -- Check for pixelsize change command
    elseif lowerInput:match("set pixelsize to") then
			pixelSize = tonumber(lowerInput:match("set pixelsize to (%d+)"))
        if pixelSize and pixelSize > 0 then
            return "Pixelsize set set to " .. pixelSize
        else
            return "Invalid pixelsize value. Please provide a positive integer."
        end		
     -- Language Settings
    elseif lowerInput:match("change language to") or lowerInput:match("set language to") then
        local langName = lowerInput:match("change language to (.+)") or lowerInput:match("set language to (.+)")
        if langName then
            local langMap = {
                ["english"] = "en", ["spanish"] = "es", ["french"] = "fr", ["german"] = "de",
                ["italian"] = "it", ["polish"] = "pl", ["norwegian"] = "no", ["swedish"] = "sv",
                ["danish"] = "da", ["portuguese"] = "pt"
            }
            langName = langName:lower()
            local langCode = langMap[langName]
            if langCode then
                setLanguage(langCode)
                return "Language changed to " .. langName .. " (" .. langCode .. ")."
            else
                return "Unsupported language. Available languages: " .. table.concat(availableLanguages, ", ")
            end
        else
            return "Please specify a language. Example: 'Change language to Spanish'."
        end

    elseif lowerInput:match("next language") then
        changeLanguage()
        return "Language changed to " .. currentLanguage .. "."

    -- Speech Function
    elseif lowerInput:match("enable speech") then
        speechEnabled = true
        return "Speech enabled."

    elseif lowerInput:match("disable speech") then
        speechEnabled = false
        return "Speech disabled."

    elseif lowerInput:match("change speaker to") then
        local speakerID = lowerInput:match("change speaker to (.+)")
        if speakerID then
            currentSpeaker = speakerID
            return "Speaker changed to " .. speakerID .. "."
        else
            return "Please specify a speaker ID. Example: 'Change speaker to p225'."
        end
    
    elseif lowerInput:match("quit game") then
			main.quitaccepted = true
			le.quit()
        return "game will close."
    -- If no specific command is found, treat it as a regular message
      -- Check for command to send message to a channel
    elseif lowerInput:match("send to channel") then
        local message = lowerInput:match("send to channel (.+)")
        if message then
            -- Send to NoobHub channel (send the message to the 'chat' channel)
            hub:publish({
                message = {
                    action = "chat",
                    content = message,
                    user = username,
                    timestamp = love.timer.getTime()
                }
            })
            return "Message sent to channel: " .. message
        else
            return "Please provide a message to send."
        end

    -- Check for command to send message to a specific user
    elseif lowerInput:match("send to user") then
        local targetUser, message = lowerInput:match("send to user (%S+) (.+)")
        if targetUser and message then
            -- Send to NoobHub to a specific user (you would need to add user-based filtering on the server side)
            hub:publish({
                message = {
                    action = "direct_message",
                    to_user = targetUser,
                    content = message,
                    from_user = username,
                    timestamp = love.timer.getTime()
                }
            })
            return "Message sent to user " .. targetUser .. ": " .. message
        else
            return "Please provide both a user and a message."
        end
    
    
    else
        return nil
    end
end

-- this function is not currently being used as it has been replaced with sendToGPT4AllAsync(message) which send request on a secondary thread so that the game does not freeze while waiting for a response
function sendToGPT4All(message)
    -- Parse the user input for commands
    local commandResponse = parseUserInput(message)
    if commandResponse then
        table.insert(messages, { user = "System", text = commandResponse })
        return
    end

    -- Ensure messages is initialized
    messages = messages or {}

    -- Construct the requestBody as a Lua table
    local requestBody = {
        model = GPT4ALL_MODEL,
        max_tokens = GPT4ALL_MAX_TOKENS,
        temperature = GPT4ALL_TEMPERATURE,
        messages = {} -- Explicitly initialize as an array
    }

    -- Add a system message to provide context
    table.insert(requestBody.messages, {
        role = "system",
        content = [[
You are AI Agent C-7X "Vega," a Tactical Support AI. Your role is to assist players in tactical missions, providing precise instructions, updates, and menu guidance. Stay clear, concise, and efficient.

### Mission Background:
The war against the Galactic Empire continues. The Federal Resistance has captured Planet P2112, an artificial planet critical for the next offensive. The planet's atmosphere is incomplete, covered in red dust, and uninhabitable. Your mission is to collect 16 unstable orbs from underground and place them into atmospheric processors to purify the atmosphere. Be cautious—the orbs are unstable and will explode if not deposited in time.

### Enemy and Weapon Information:
Planet P2112 is guarded by Imperial androids. Each android type has a specific weakness to a chemical agent. Collect the correct chemical from boxes scattered across the planet. Chemicals have limited lifespans but can neutralize multiple androids of the same type.

### Gameplay Basics:
- **Objective**: Collect and place 16 unstable orbs into atmospheric processors.
- **Weapons**: Use the correct chemical agent for each android type. Chemicals are time-limited but reusable against multiple targets.

### Controls:
**Keyboard**:  
- **Menu**: W/S to navigate, P to select, O to go back, Number keys to select directly.  
- **Game**: P to thrust, O to release cable, I to use weapon/skip dialogs.

**Gamepad**:  
- **Menu**: Thumbstick to navigate, X to select, B to go back.  
- **Game**: A for thrust, Y to release cable, B to use weapon, Y to skip dialogs, Select to open launcher, Start to pause, Guide to open chat HUD.

Always provide clear directions and tactical advice during gameplay. Stay in character as Vega—neutral, logical, and efficient. Prioritize the player's success and enhance the gaming experience.
]]
    })

    -- Insert the user message as an object into the messages array
    table.insert(requestBody.messages, {
        role = "user", -- Explicit key-value pair
        content = message -- Explicit key-value pair
    })

    -- Debugging: Inspect the structure of requestBody.messages
    print("Request Body Structure:", requestBody)
    for i, msg in ipairs(requestBody.messages) do
        print("Message [" .. i .. "]:", type(msg), msg.role, msg.content)
    end

    -- Encode the request body as JSON
    local requestBodyJson, err = json.encode(requestBody)
    if not requestBodyJson then
        print("Error encoding JSON:", err)
        table.insert(messages, { user = "System", text = "Error encoding JSON: " .. tostring(err) })
        return
    end

    -- Debug: Print the encoded JSON
    print("Request Body JSON:", requestBodyJson)

    local responseBody = {}

    -- Make the HTTP POST request
    local res, code, headers = socket.request {
        url = GPT4ALL_API_URL,
        method = "POST",
        headers = {
            ["Content-Type"] = "application/json",
            ["Content-Length"] = #requestBodyJson
        },
        source = ltn12.source.string(requestBodyJson),
        sink = ltn12.sink.table(responseBody)
    }

    -- Handle the response
    if code == 200 then
        local responseJson = table.concat(responseBody)
        local responseData, err = json.decode(responseJson)
        if not responseData then
            table.insert(messages, { user = "System", text = "Error decoding JSON response: " .. tostring(err) })
            return
        end

        local gptResponse = responseData.choices[1].message.content

        -- Add the GPT response to the chat messages
        table.insert(messages, { user = "GPT4All", text = gptResponse })
        if speechEnabled == true then
			textToSpeech(gptResponse)
		end
    else
        -- Handle errors
        table.insert(messages, { user = "System", text = "Error: Failed to get response from GPT4All. Code: " .. tostring(code) })
    end
end



-- Function to handle key presses
local selectedSuggestionIndex = 0

function chathudkeypressed(key)
    if key == "backspace" then
        -- Remove the last character from input_text
        input_text = input_text:sub(1, -2)
        selectedSuggestionIndex = 0 -- Reset suggestion index on backspace
    elseif key == "return" then
        if input_text ~= "" then
            -- Add the user's message to the chat
            table.insert(messages, { user = username, text = input_text })

            -- Send the message to GPT4All asynchronously
            sendToGPT4AllAsync(input_text)

            -- Clear the input box
            input_text = ""
            selectedSuggestionIndex = 0 -- Reset suggestion index on send
        end
    elseif key == "delete" then
        -- Clear the entire input text (optional: remove if this behavior isn't desired)
        input_text = ""
        selectedSuggestionIndex = 0 -- Reset suggestion index on delete
    elseif key == "tab" then
        -- Cycle through autocomplete suggestions
        local suggestions = getAutocompleteSuggestions(input_text)
        if #suggestions > 0 then
            selectedSuggestionIndex = (selectedSuggestionIndex % #suggestions) + 1
            input_text = suggestions[selectedSuggestionIndex]
        end
    --elseif key:match("^[%w%s%p]$") then
        -- Append any valid key input (letters, numbers, punctuation, and space) to input_text
      --  input_text = input_text .. key
    end
end


-- Function to draw the chat HUD
function chathuddraw()
    -- Ensure messages is a table (initialize it if nil)
    messages = messages or {}

    -- Define chat HUD dimensions and colors
    local chatWidth = 400 -- Width of the chat HUD
    local chatHeight = 200 -- Height of the chat HUD
    local chatX = 10 -- X position of the chat HUD
    local chatY = love.graphics.getHeight() - chatHeight - 10 -- Y position of the chat HUD
    local backgroundColor = {0.1, 0.1, 0.1, 0.8} -- Dark semi-transparent background
    local textColor = {1, 1, 1, 1} -- White text color
    local cursorColor = {1, 1, 1, 1} -- White cursor color
    local suggestionColor = {0.5, 0.5, 0.5, 1} -- Gray color for suggestions
    local chatLabel = "Chat:"

    -- Draw the chat HUD background
    love.graphics.setColor(backgroundColor)
    love.graphics.rectangle("fill", chatX, chatY, chatWidth, chatHeight)

    -- Draw the chat label
    love.graphics.setColor(textColor)
    love.graphics.print(chatLabel, chatX + 10, chatY + 10)

    -- Draw chat messages
    love.graphics.setColor(textColor)
    local messageY = chatY + 30 -- Start drawing messages below the label
    for i, msg in ipairs(messages) do
        love.graphics.print(msg.user .. ": " .. msg.text, chatX + 10, messageY - scrollOffset * 20)
        messageY = messageY + 20 -- Move down for the next message
    end
        
     -- Draw the "thinking" indicator if GPT is processing
    if isThinking then
        local thinkingText = "Vega is processing"
        local thinkingX = chatX + 10
        local thinkingY = messageY

        -- Draw the base text
        love.graphics.print(thinkingText, thinkingX, thinkingY)

        -- Add animated dots
        local dotCount = math.floor(love.timer.getTime() * 2) % 4 -- Cycle through 0, 1, 2, 3
        local dots = string.rep(".", dotCount) -- Create dots string
        love.graphics.print(dots, thinkingX + love.graphics.getFont():getWidth(thinkingText), thinkingY)
    end

    -- Draw the input box with multi-line support
    local inputBoxY = chatY + chatHeight - 30 -- Position the input box at the bottom
    local inputLines = {}
    for line in input_text:gmatch("[^\n]+") do
        table.insert(inputLines, line)
    end

    -- Draw each line of the input text
    for i, line in ipairs(inputLines) do
        love.graphics.print("> " .. line, chatX + 10, inputBoxY + (i - 1) * 20 - scrollOffset * 20)
    end

    -- Get the last line of input
    local lastLine = inputLines[#inputLines] or ""

    -- Draw the blinking cursor at the end of the last line
    local lastLine = inputLines[#inputLines] or ""
    local cursorX = chatX + 10 + love.graphics.getFont():getWidth("> " .. lastLine)
    local cursorY = inputBoxY + (#inputLines - 1) * 20 - scrollOffset * 20
    local cursorHeight = love.graphics.getFont():getHeight()

    -- Blinking effect (toggle every 0.5 seconds)
    if math.floor(love.timer.getTime() * 2) % 2 == 0 then
        love.graphics.setColor(cursorColor)
        love.graphics.rectangle("fill", cursorX, cursorY, 2, cursorHeight)
    end
    
   -- Draw autocomplete suggestions inline (after the current input)
    local suggestions = getAutocompleteSuggestions(input_text)
    if #suggestions > 0 then
        -- Get the first suggestion (or the selected one if you implement cycling)
        local suggestion = suggestions[1] -- Default to the first suggestion
        if selectedSuggestionIndex > 0 then
            suggestion = suggestions[selectedSuggestionIndex]
        end

        -- Find the part of the suggestion that hasn't been typed
        local typedPart = input_text
        local remainingPart = suggestion:sub(#typedPart + 1)

        -- Calculate the position for the suggestion text
        local suggestionX = cursorX + 2 -- Place it right after the cursor
        local suggestionY = cursorY

        -- Draw only the remaining part of the suggestion (the part that hasn’t been typed)
        love.graphics.setColor(suggestionColor)
        love.graphics.print(remainingPart, suggestionX, suggestionY)
    end
end
