-- touchKeyboard.lua

touchKeyboard = {}

-- Define the keyboard layout
--[[keyboardLayout = {
     {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0","del"},
    {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P","ret"},
    {"A", "S", "D", "F", "G", "H", "J", "K", "L", ""},
    {"Z", "X", "C", "V", "B", "N", "M", ".", "", ""},
    {"", "", "", "", "", "", "", "", "", ""}

}
--]]

keyboardLayout = {
    {"1", "2", "3", "del"},
    {"4", "5", "6", "ret"},
    {"7", "8", "9", ""},
    {".", "0", "", ""}
}


-- Define the keyboard layout
keyboardLayoutLandscape = {
    {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0","del"},
    {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P","ret"},
    {"A", "S", "D", "F", "G", "H", "J", "K", "L", ""},
    {"Z", "X", "C", "V", "B", "N", "M", ".", "", ""},
    {"", "", "", "", "", "", "", "", "", ""}

}

KeyboardOrientation= "portrait"

-- Function to select the appropriate keyboard layout based on orientation
function selectKeyboardLayout()
    if KeyboardOrientation == "landscape" then
        return keyboardLayoutLandscape
    else
        return keyboardLayout
    end
end

-- Define the position and size of the keyboard
--keyboardX = 50
--keyboardY = 100
keyWidth = 60*3
keyHeight = 60*3
keyPadding = 10*3


-- Initialize the virtual keyboard
function touchKeyboard:init()
    self.selectedKey = nil
    love.graphics.setFont(fonts.default)
end


require ("touchControls/Keyboard/keyboardUpdate")
require ("touchControls/Keyboard/keyboardDraw")




function touchKeyboard:touchpressed(x, y)
    print("Touch pressed at coordinates:", x, y)
    if x==nil then x=0 end
    if y==nil then y=0 end
    if self.rotation==nil then self.rotation=0 end
    -- Apply the inverse rotation to the touch coordinates
    local rotatedX, rotatedY = rotatePoint(x - self.x, y - self.y, -self.rotation)
    --print(rotatedX)
    
    -- Iterate over the keys and check for press
    local layout = selectKeyboardLayout() -- Select the appropriate keyboard layout
    for i, row in ipairs(layout) do
        for j, key in ipairs(row) do
            local keyX = (j - 1) * (keyWidth + keyPadding)
            local keyY = (i - 1) * (keyHeight + keyPadding)
            
            -- Check if the rotated coordinates are within the key boundaries
            if rotatedX >= keyX and rotatedX <= keyX + keyWidth and
               rotatedY >= keyY and rotatedY <= keyY + keyHeight then
                print("Selected key:", key)
                self.selectedKey = key
                
                -- Add mobile vibration (if supported)
                love.system.vibrate(0.1)
                
                return
            end
        end
    end
    
    -- If no key is selected
    print("No key selected")
end




-- Function to rotate a point (x, y) around the origin by angle theta
function rotatePoint(x, y, theta)
    local cosTheta = math.cos(theta)
    local sinTheta = math.sin(theta)
    local rotatedX = x * cosTheta - y * sinTheta
    local rotatedY = x * sinTheta + y * cosTheta
    return rotatedX, rotatedY
end

function touchKeyboard:mousepressed(x, y, button, istouch, presses)
    --if button == 1 then -- Left mouse button
        self:touchpressed(x, y)
    --end
    
end

function touchKeyboard:mousereleased(x, y, button, istouch, presses)
    --if button == 1 then -- Left mouse button
        self:touchreleased()
    --end
end


-- Handle touchreleased event
function touchKeyboard:touchreleased()
    self.selectedKey = nil
end

-- Return the selected key
function touchKeyboard:getSelectedKey()
		return self.selectedKey
end

return touchKeyboard
