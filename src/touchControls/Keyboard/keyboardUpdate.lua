-- Define constants for keyboard dimensions and padding
local landscapeKeyboardX = 150
local landscapeKeyboardY = 1200
local portraitKeyboardX = 1000
local portraitKeyboardY = 1050
--local keyWidth = 50
--local keyHeight = 50
local keyPadding = 5

-- Determine the orientation and set keyboard position accordingly
function updateKeyboardPosition()
    local width, height = love.graphics.getDimensions()
     touchKeyboard.x = landscapeKeyboardX
        touchKeyboard.y = landscapeKeyboardY
        touchKeyboard.rotation = 0
    if KeyboardOrientation=="portrait" then -- Landscape orientation
        touchKeyboard.x = landscapeKeyboardX
        touchKeyboard.y = landscapeKeyboardY
        touchKeyboard.rotation = 0
    elseif KeyboardOrientation=="landscape" then
        touchKeyboard.x = portraitKeyboardX
        touchKeyboard.y = portraitKeyboardY
        touchKeyboard.rotation = math.pi / 2 -- Rotate 90 degrees
    end
end


-- Initialize a variable to track the previously pressed key
--local previousKey = nil

local typingCooldown = 0.2  -- Cooldown duration in seconds
local lastKeyTime = {}  -- Table to store the time when each key was last pressed

function updateKeyboard(dt)
    --updateKeyboardPosition()
    local selectedKey = nil  -- Initialize selectedKey variable

    -- Check if the layout is keyboard or mouse
    if touchControls.layout == "keyboard" then
        -- Get current touches or mouse click
        local touches = love.touch.getTouches()
        if #touches == 0 and love.mouse.isDown(1) then -- Check if left mouse button is clicked
            -- Get mouse position
            local touchX, touchY = love.mouse.getPosition()
            touchKeyboard:touchpressed(touchX, touchY)
            --print("Mouse Position:", touchX, touchY)
            -- Call touchpressed and touchreleased methods of touchKeyboard or handle mouse input
        else
            -- Iterate over each touch
            for i, touchID in ipairs(touches) do
                -- Get touch position for each touch
                local touchX, touchY = love.touch.getPosition(touchID)
                --print("Touch Position:", touchX, touchY)
                -- Call touchpressed and touchreleased methods of touchKeyboard
            end
        end

        -- Call getSelectedKey to retrieve the selected key
        selectedKey = touchKeyboard:getSelectedKey()

        -- Check if a key is selected and if enough time has passed since the last key was pressed
        if selectedKey then
            local currentTime = love.timer.getTime()
            local lastPressTime = lastKeyTime[selectedKey] or 0
            
            if currentTime - lastPressTime > typingCooldown then
                print("Selected Key:", selectedKey)
                typingSound:play()
                love.textinput(selectedKey)

                -- Update the time the key was last pressed
                lastKeyTime[selectedKey] = currentTime
            end
        end
    end

    return selectedKey -- Return selectedKey
end


