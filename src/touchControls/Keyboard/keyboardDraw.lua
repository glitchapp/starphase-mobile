
touchKeyboard.x=50
touchKeyboard.y=1400
-- Draw the virtual keyboard
function touchKeyboard:draw()
    
    if self.rotation==nil then self.rotation=0 end
    --love.graphics.push() -- Save the current transformation
    love.graphics.translate(self.x, self.y) -- Translate to the keyboard position
    love.graphics.rotate(self.rotation) -- Rotate the keyboard

    local layout = selectKeyboardLayout() -- Select the appropriate keyboard layout

    for i, row in ipairs(layout) do
        for j, key in ipairs(row) do
            local keyX = (j - 1) * (keyWidth + keyPadding)
            local keyY = (i - 1) * (keyHeight + keyPadding)

            -- Fill the button with color if it's pressed
            if self.selectedKey == key then
                love.graphics.setColor(0.2, 0.2, 0.8) -- Blue color
                love.graphics.rectangle("fill", keyX, keyY, keyWidth, keyHeight)
                love.graphics.setColor(1, 1, 1) -- Reset color
            else
                love.graphics.setColor(0.4, 0.4, 0.4) -- Default button color
                love.graphics.rectangle("line", keyX, keyY, keyWidth, keyHeight)
            end

		love.graphics.setFont(fonts.paused_large)
            -- Draw the key label
            love.graphics.print(key, keyX + keyWidth / 2 - 5, keyY + keyHeight / 2 - 5)
        end
    end
	
    --love.graphics.pop() -- Restore the previous transformation
end
