-- touchControls.lua

require ("touchControls/Menu/Buttons")
require ("touchControls/Menu/DrawButton")

touchControls = {
  leftThumbstickRadius = 240,
  rightThumbstickRadius = 240,
  buttonRadius = 150,
  buttonDistance = 160,
  leftxaxis = 0,
  leftyaxis = 0,
  rightxaxis = 0,
  rightyaxis = 0,
  leftThumbstickPressed = false,
  rightThumbstickPressed = false,
  buttons = {}
}

touchControls.layout="arcade"

-- Enum for layout types
local LayoutType = {
	GAMEPAD = 1,
    ARCADE = 2,
    KEYBOARD = 3, -- Add KEYBOARD layout
}


require ("touchControls/Gamepad/checkButtonPress")
require ("touchControls/Gamepad/createGamepadButtons")
require ("touchControls/Gamepad/createArcadeButtons")
require ("touchControls/Gamepad/removeLeftThumbstickButtons")
require ("touchControls/Gamepad/removeRightThumbstickButtons")
require ("touchControls/Gamepad/toggleLayout")
require ("touchControls/Gamepad/touchEvents")
require ("touchControls/Gamepad/mouseEvents")
require ("touchControls/Menu/About")
require ("touchControls/Menu/checkMenuButtons")
require ("touchControls/Menu/IsHovered")
require ("touchControls/Menu/NetworkSetup")






--require ("src/Network/sendAxes")


function touchControls:init()


	
	   --if MobileOrientation == "landscape" then
  self.leftThumbstickX = love.graphics.getWidth() / 5.2
  self.leftThumbstickY = love.graphics.getHeight() / 2.2

            
            --[[self.leftThumbstickX = love.graphics.getWidth() / 2.2
            self.leftThumbstickY = 2340/2.6

            self.rightThumbstickX = love.graphics.getWidth() *2
            self.rightThumbstickY = 2340/2.6
            --]]
        --else
            --self.leftThumbstickX = love.graphics.getWidth() / 4
            --self.leftThumbstickY = love.graphics.getHeight() / 2.2

            --self.rightThumbstickX = love.graphics.getWidth() - 600
            --self.rightThumbstickY = love.graphics.getHeight() / 2.2
        --end
 
    self:createArcadeButtons()
    self.rightThumbstickPressed = false
    --self.rightxaxis = 0
    --self.rightyaxis = 0
  

end


function initParametersTouch()


touchControls.leftThumbstickRadius = 140
touchControls.rightThumbstickRadius = 240
touchControls.buttonRadius = 100
touchControls.buttonDistance = 160
touchControls.leftxaxis = 0
touchControls.leftyaxis = 0
touchControls.rightxaxis = 0
touchControls.rightyaxis = 0
touchControls.leftThumbstickPressed = false
touchControls.rightThumbstickPressed = false
touchControls.leftThumbstickY = love.graphics.getHeight() / 1.2
  buttons = {}

  touchControls:createArcadeButtons()
end

require ("touchControls/Gamepad/updateThumbstickAxes")
require ("touchControls/Gamepad/checkButtonPress")
require ("touchControls/Gamepad/drawTouchControls")



-- Return touchControls table
return touchControls

