-- Load LuaSocket library
--socket = require("socket")

-- Common base port number
--local basePort = 1024
	--receivePort=1024
	--sendPort=1025


-- Set the initial instance number
instanceNumber = 1

--[[
-- Function to handle keyboard input
function love.keypressed(key)
    -- If the key pressed is "1", set instanceNumber to 1
    if key == "1" then
        instanceNumber = 1
        	receivePort=1024
			sendPort=1025
        print("Instance selected: 1")
    end

    -- If the key pressed is "2", set instanceNumber to 2
    if key == "2" then
        instanceNumber = 2
			receivePort=1025
			sendPort=1024
        print("Instance selected: 2")
    end
end
--]]

--initialize variables
touchControls.receivedLeftXAxis,touchControls.receivedLeftYAxis=0,0
touchControls.receivedRightXAxis,touchControls.receivedRightYAxis=0,0

-- Function to determine the instance number
local function getInstanceNumber()
  return instanceNumber
end


-- Determine the send and receive ports based on the instance number
--local sendPort = basePort + instanceNumber
--local receivePort = basePort + (instanceNumber % 2) + 1 -- Ensure different receive port for each instance

-- Create a UDP socket
udp = socket.udp4()
udp:settimeout(0)


math.randomseed(os.time()) 
	entity = tostring(math.random(99999))

-- Define variables to store received axis values
receivedLeftXAxis = 0
receivedLeftYAxis = 0
receivedRightXAxis = 0
receivedRightYAxis = 0

function receiveThumbstickAxes()

  -- Print a message to indicate that the function is called
  --print("Receiving thumbstick axes...")

  -- Attempt to receive data from the UDP socket
  --data, address, port = udp:receivefrom()
  data, address, port = udp:receivefrom()
  
  --local data = udp:receivefrom()

  -- Print the received IP address and port for debugging
  --print("Received from IP:", address)
  --print("Received from Port:", port)
	--print("Received data:", data)

  -- Check if data is received
  if data then
  
  
	offsetX,offsetY=200,100
	offsetXright,offsetYright=400,100



    --print("Received data:", data)

    -- Deserialize the received data
    local values = {}
    for value in string.gmatch(data, "([^,]+)") do
      table.insert(values, tonumber(value))
    end

	--debug of data

-- Print each value
for i, value in ipairs(values) do
    print("Value " .. i .. ": " .. value)
		if i==1 then touchControls.receivedLeftXAxis=value 
    elseif i==2 then touchControls.receivedLeftYAxis=value 
    elseif i==3 then touchControls.receivedRightXAxis=value 
    elseif i==4 then touchControls.receivedRightYAxis=value 
    end
end
	
    -- Print the parsed values for debugging
    --print("Parsed values:", unpack(values))

--normalize values
--[[
    -- Update thumbstick axes based on the received data
    if #values >= 2 then
      --receivedLeftXAxis = normalizeAxisValue(values[1])
      --receivedLeftYAxis = normalizeAxisValue(values[2])
      --print("Updated Left Thumbstick Axes - X:", receivedLeftXAxis, "Y:", receivedLeftYAxis)
    end

    if #values >= 4 then
      --receivedRightXAxis = normalizeAxisValue(values[3])
      --receivedRightYAxis = normalizeAxisValue(values[4])
      --print("Updated Right Thumbstick Axes - X:", receivedRightXAxis, "Y:", receivedRightYAxis)
    end
    --]]
    --touchControls:updateReceivedLeftThumbstickAxes(receivedLeftXAxis, receivedLeftYAxis)
	--touchControls:updateReceivedRightThumbstickAxes(receivedRightXAxis, receivedRightYAxis)
	
	print("data received.")
  else
  	offsetX,offsetY=0,0
	offsetXright,offsetYright=0,0
    -- Print a message if no data is received
    --print("No data received.")
  end
end



-- Function to normalize and scale axis values to fit within the range of -1 to 1
function normalizeAxisValue(value)
  -- Assuming the range of received axis values is between 0 and 1,
  -- you can normalize and scale them to fit within the range of -1 to 1
  return (value * 2) - 1
end

