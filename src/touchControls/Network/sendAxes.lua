-- Load LuaSocket library
--socket = require("socket")

-- Common base port number
--local basePort = 1024
	--receivePort=1024
	--sendPort=1025

-- Set the initial instance number


-- Define a variable to track whether the identifier has been sent
local identifierSent = false



--[[
-- Function to handle keyboard input
function love.keypressed(key)
    -- If the key pressed is "1", set instanceNumber to 1
    if key == "1" then
        instanceNumber = 1
        	receivePort=1024
			sendPort=1025
        print("Instance selected: 1")
    end

    -- If the key pressed is "2", set instanceNumber to 2
    if key == "2" then
        instanceNumber = 2
			receivePort=1025
			sendPort=1024
        print("Instance selected: 2")
    end
end
--]]
-- Function to determine the instance number
local function getInstanceNumber()
  return instanceNumber
end

function sendIdentifier()
-- Serialize the thumbstick axes data
    local data = ""
    
    
        -- Add the instance identifier as the first value
        data = data .. tostring(instanceType) .. ","
        print(data)
        identifierSent = true
         udp:sendto(data, sendAddress, sendPort)
		--print(instanceType)
    --data = data .. string.format("%f,%f,%f,%f", leftxaxis, leftyaxis, rightxaxis, rightyaxis)
end


-- Determine the send and receive ports based on the instance number
--local sendPort = basePort + instanceNumber
--local receivePort = basePort + (instanceNumber % 2) + 1 -- Ensure different receive port for each instance

-- Create a UDP socket
udp = socket.udp4()
udp:settimeout(0)


-- Set the destination IP address and port for sending
--sendAddress = "127.0.0.1"
sendAddress = "192.168.178.36"

-- Set the local IP address and port for receiving
--receiveAddress = "127.0.0.1"
--receiveAddress = "192.168.178.36"
receiveAddress, receivePort = udp:getsockname()

udp:setsockname(receiveAddress, receivePort)

-- Function to send thumbstick axes data
function sendThumbstickAxes(leftxaxis, leftyaxis, rightxaxis, rightyaxis)
  -- Initialize variables if they are nil
  leftxaxis = leftxaxis or 0
  leftyaxis = leftyaxis or 0
  rightxaxis = rightxaxis or 0
  rightyaxis = rightyaxis or 0
  
  -- Serialize the thumbstick axes data
  local data = string.format("%f,%f,%f,%f", touchControls.leftxaxis, touchControls.leftyaxis, touchControls.rightxaxis, touchControls.rightyaxis)
	
	--debug of data
	--[[
	-- Split the string into individual values
local values = {}
for value in data:gmatch("([^,]+)") do
    table.insert(values, tonumber(value))
end

-- Print each value
for i, value in ipairs(values) do
    print("Value " .. i .. ": " .. value)
end
	--]]
  -- Send data via UDP socket to the specified IP address and port
  udp:sendto(data, sendAddress, sendPort)
  --print("Instance", instanceNumber, "sent data:", data)
  --print(data[1])
  -- Print the ports
--print("Instance", instanceNumber, "sending on port:", sendPort)
--print("Instance", instanceNumber, "listening on port:", receivePort)
end

