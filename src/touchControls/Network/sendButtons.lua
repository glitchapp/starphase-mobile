-- Function to send thumbstick axes data
function sendButtons(btnIndex, btnPressed)
  -- Check if both parameters are valid
  if btnIndex and btnPressed ~= nil then
    -- Serialize the buttons data
    local data = string.format("%d,%d", btnIndex, btnPressed and 1 or 0)
    
    -- Send data via UDP socket to the specified IP address and port
    udp:sendto(data, sendAddress, sendPort)
  else
    --print("Invalid parameters for sending buttons data.")
  end
end
