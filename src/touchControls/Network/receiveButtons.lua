 -- Initialize btnPressed to 0 by default
  btnPressed = 0
-- Function to receive button press information over UDP
function receiveButtons()
  -- Attempt to receive data from the UDP socket
  data, address, port = udp:receivefrom()
  
  -- Ensure btnPressed is valid
  --btnPressed = btnPressed and 1 or 0
  
  -- Check if data is received
  if data then
    -- Deserialize the received data
    local btnIndex, btnPressed = data:match("([^,]+),([^,]+)")
	
    -- Convert button index and pressed state to appropriate types
    btnIndex = tonumber(btnIndex)
    btnPressed = btnPressed == "true"
	
    -- Print the received data for debugging
    --print("Received button press - Index:", btnIndex, "Pressed:", btnPressed)
	
    -- Handle the button press accordingly
    -- For example:
    -- if btnPressed then
    --   -- Execute actions corresponding to the button press
    -- else
    --   -- Execute actions corresponding to releasing the button
    -- end
  else
    -- Print a message if no data is received
    --print("No button press data received.")
  end
end
