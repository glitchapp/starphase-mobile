-- Register touchpressed event for left and right thumbsticks
function love.touchpressed(id, x, y, dx, dy, pressure)
    -- Adjust coordinates based on the current orientation
    if MobileOrientation == "landscape" then
        x, y = love.graphics.inverseTransformPoint(x, y)
    end

    if not (touchControls.leftThumbstickX == nil) then
        local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX) ^ 2 + (y - touchControls.leftThumbstickY) ^ 2)
        --local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX) ^ 2 + (y - touchControls.rightThumbstickY) ^ 2)

        if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
            touchControls.leftThumbstickPressed = true
            touchControls:updateLeftThumbstickAxes(x, y)
        --[[elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout == "arcade") then
            touchControls.rightThumbstickPressed = true
            touchControls:updateRightThumbstickAxes(x, y)
            --]]
        else
            touchControls.leftThumbstickPressed = false
            if not (layout == "arcade") then
                touchControls.rightThumbstickPressed = false
            end
          
        end
    end
      touchControls:checkButtonPress(x, y)
end

-- Register touchmoved event for left and right thumbsticks
function love.touchmoved(id, x, y, dx, dy, pressure)
    -- Adjust coordinates based on the current orientation
    if MobileOrientation == "landscape" then
        x, y = love.graphics.inverseTransformPoint(x, y)
    end

    if touchControls.leftThumbstickPressed then
        touchControls:updateLeftThumbstickAxes(x, y)
    --[[elseif touchControls.rightThumbstickPressed and not (layout == "arcade") then
        touchControls:updateRightThumbstickAxes(x, y)
       --]]
    else
        -- Handle other touchmoved logic
    end
end

-- Register touchreleased event for left and right thumbsticks
function love.touchreleased(id, x, y, dx, dy, pressure)
    -- Adjust coordinates based on the current orientation
    if MobileOrientation == "landscape" then
        x, y = love.graphics.inverseTransformPoint(x, y)
    end

    if not (touchControls.leftThumbstickX == nil) then
        local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX) ^ 2 + (y - touchControls.leftThumbstickY) ^ 2)
        --local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX) ^ 2 + (y - touchControls.rightThumbstickY) ^ 2)

        if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
            touchControls.leftThumbstickPressed = false
            touchControls.leftxaxis = 0
            touchControls.leftyaxis = 0
        --[[elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout == "arcade") then
            touchControls.rightThumbstickPressed = false
            touchControls.rightxaxis = 0
            touchControls.rightyaxis = 0
           --]]
        end
    end
        
        button0or4Pressed=false
        button2or6Pressed=false
        button3or7Pressed=false
        button1or5Pressed=false
        
             -- Reset the pressed state for all buttons
    for i, button in pairs(touchControls.buttons) do
        button.pressed = false
    end
end
