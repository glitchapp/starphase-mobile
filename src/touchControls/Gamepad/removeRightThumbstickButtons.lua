
-- Add a new function to remove buttons associated with the right thumbstick
function touchControls:removeRightThumbstickButtons()
  --[[local buttonsToRemove = {}
  for i, button in ipairs(self.buttons) do
    local distanceToRightThumbstick = math.sqrt((button.x - self.rightThumbstickX)^2 + (button.y - self.rightThumbstickY)^2)
    if distanceToRightThumbstick <= self.rightThumbstickRadius + self.buttonRadius then
      table.insert(buttonsToRemove, i)
    end
  end

  -- Remove buttons associated with the right thumbstick
  for i = #buttonsToRemove, 1, -1 do
    table.remove(self.buttons, buttonsToRemove[i])
  end
  --]]
end
