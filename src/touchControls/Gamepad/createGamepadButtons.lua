-- Create buttons for gamepad layout
function touchControls:createGamepadButtons()
  -- Create buttons around the left thumbstick
  local buttonCount = 4
  local angleIncrement = 2 * math.pi / buttonCount

  for i = 1, buttonCount do
    local angle = (i - 1) * angleIncrement
    local buttonX = self.leftThumbstickX + (self.leftThumbstickRadius + self.buttonDistance) * math.cos(angle)
    local buttonY = self.leftThumbstickY + (self.leftThumbstickRadius + self.buttonDistance) * math.sin(angle)

    table.insert(self.buttons, {x = buttonX, y = buttonY, pressed = false})
  end

  -- Create buttons around the right thumbstick
  self:createRightThumbstickButtons()
end


-- Update the existing createButtons function to create buttons around the left thumbstick
function touchControls:createButtons()
  local buttonCount = 4
  local angleIncrement = 2 * math.pi / buttonCount

  for i = 1, buttonCount do
    local angle = (i - 1) * angleIncrement
    local buttonX = self.leftThumbstickX + (self.leftThumbstickRadius + self.buttonDistance) * math.cos(angle)
    local buttonY = self.leftThumbstickY + (self.leftThumbstickRadius + self.buttonDistance) * math.sin(angle)

    table.insert(self.buttons, {x = buttonX, y = buttonY, pressed = false})
  end

  -- Create buttons around the right thumbstick
  self:createRightThumbstickButtons()
end

-- Add a new function to create buttons around the right thumbstick
function touchControls:createRightThumbstickButtons()
  local buttonCount = 4
  local angleIncrement = 2 * math.pi / buttonCount

  for i = 1, buttonCount do
    local angle = (i - 1) * angleIncrement
    local buttonX = self.rightThumbstickX + (self.rightThumbstickRadius + self.buttonDistance) * math.cos(angle)
    local buttonY = self.rightThumbstickY + (self.rightThumbstickRadius + self.buttonDistance) * math.sin(angle)

    table.insert(self.buttons, {x = buttonX, y = buttonY, pressed = false})
  end
end
