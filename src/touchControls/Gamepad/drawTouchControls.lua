
-- Update the existing draw function to draw the right thumbstick and its virtual thumbstick
function touchControls:draw()
  -- Draw left thumbstick
  love.graphics.circle("line", self.leftThumbstickX, self.leftThumbstickY, self.leftThumbstickRadius)

  -- Draw virtual thumbstick for left thumbstick when pressed and dragged
  if self.leftThumbstickPressed then
    love.graphics.circle("line", self.leftThumbstickX + touchControls.leftxaxis, self.leftThumbstickY + touchControls.leftyaxis, self.leftThumbstickRadius * 1.5)
   --print("Drawing Virtual Thumbstick - Left Thumbstick Pressed - X Axis:", touchControls.leftxaxis, "Y Axis:", touchControls.leftyaxis) -- Debug statement
  end

  -- Draw buttons for left thumbstick
  for i, button in pairs(self.buttons) do
			if i==6 then love.graphics.setColor(1,0,0,1)
		elseif i==2 then love.graphics.setColor(1,0,0,1)
		
		elseif i==3 then love.graphics.setColor(0,0,1,1)
		elseif i==7 then love.graphics.setColor(0,0,1,1)
		
		elseif i==4 then love.graphics.setColor(0,1,0,1)
		elseif i==8 then love.graphics.setColor(0,1,0,1)
		
		elseif i==1 then love.graphics.setColor(0,1,1,1)
		elseif i==5 then love.graphics.setColor(0,1,1,1)
		
		else love.graphics.setColor(1,1,1,1)
		end
  
    if button.pressed then
      love.graphics.circle("fill", button.x, button.y, self.buttonRadius)
    else
      love.graphics.circle("line", button.x, button.y, self.buttonRadius)
    end
  end


	if not (layout=="arcade") then
  -- Draw right thumbstick
	--[[	
			love.graphics.circle("line", self.rightThumbstickX, self.rightThumbstickY, self.rightThumbstickRadius)
	
  -- Draw virtual thumbstick for right thumbstick when pressed and dragged
  if self.rightThumbstickPressed then
			if KeyboardOrientation == "portrait" then
				love.graphics.circle("line", self.rightThumbstickX + touchControls.rightxaxis, self.rightThumbstickY + touchControls.rightyaxis, self.rightThumbstickRadius * 1.5)
		elseif KeyboardOrientation == "landscape" then
				love.graphics.circle("line", self.rightThumbstickY + touchControls.rightyaxis, self.rightThumbstickX + touchControls.rightxaxis, self.rightThumbstickRadius * 1.5)
		end
  end
  --]]
  end
   
   --[[-- Draw layout switch button
  local switchButtonX = love.graphics.getWidth() - 100
  local switchButtonY = 20
  local switchButtonWidth = 80
  local switchButtonHeight = 30

  love.graphics.setColor(0.4, 0.4, 0.4)
  love.graphics.rectangle("fill", switchButtonX, switchButtonY, switchButtonWidth, switchButtonHeight)
  love.graphics.setColor(1, 1, 1)
  --]]
  
  --love.graphics.print("Switch Layout", switchButtonX + 10, switchButtonY + 10)
  
  --love.graphics.print("CurrentLayout: " .. touchControls.layout, 200,50,0,1)

  
end
