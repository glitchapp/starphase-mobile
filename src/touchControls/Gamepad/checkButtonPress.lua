			if not title.activemenu then
                    title.activemenu = {}  -- Initialize activemenu if not defined
                    print("activemenu initialized")  -- Debug statement
                end
function touchControls:checkButtonPress(x, y)
	if x==nil then x=0 end
	if y==nil then y=0 end
	
	local dt = love.timer.getTime() - (self.previousTime or love.timer.getTime())
    self.previousTime = love.timer.getTime()
	 --print("Mode:", mode)  -- Debug statement to check the value of 'mode'

   -- Debug statement to check if title.activemenu exists
    --[[if title and title.activemenu then
        print("title.activemenu exists")
    else
        print("title.activemenu does not exist")
    end
	--]]
  for i, button in pairs(self.buttons) do
    local distanceToButton = math.sqrt((x - button.x)^2 + (y - button.y)^2)

     if distanceToButton <= self.buttonRadius then
      button.pressed = true
       
 
            if mode == "title" then
					if i==0 or i==4 then
						title:TouchButtonPressed(dt)
				elseif i==2 or i==6 then
						title:TouchButtonPressed(dt)
				elseif i==3 or i==7 then
						title:TouchButtonPressed(dt)
				elseif i==1 or i==5 then
						title:TouchButtonPressed(dt)
					end
	elseif mode == "arcade" then
					
				if i==0 or i==4 then
						button0or4Pressed=true
						player:TouchShoot(dt)
			elseif i==2 or i==6 then
						button2or6Pressed=true 
						player:TouchShoot(dt)
			elseif i==3 or i==7 then
						button3or7Pressed=true
			elseif i==1 or i==5 then
						button1or5Pressed=true
			end
			
	elseif mode == "editing" then
			
	end
	
	  
    end
  end
end


function title:TouchButtonPressed()
	if #load.files > 0 then return end
	if debugstarfield then
		
			title.menu.selected = 4
			debugstarfield = false
			button2or6Pressed=false
			love.timer.sleep(0.1)
		
		return
	end
	
		
	if title.splash then 
		--if key == "space" then 
	
			title.splash = false 
			button0or4Pressed=false
			love.timer.sleep(0.1)
			return 
	end
	
		


		sound:play(title.sounds.select)
				
		if title.active == "main" then
			if title.menu.selected == 1 then title.active = "ship_selection" title.menu.maxoptions = 4 end
			if title.menu.selected == 2 then title.active = "ship_selection" title.menu.maxoptions = 4 twoplayers=true end
			if title.menu.selected == 3 then initdebugarcade(3) end
			if title.menu.selected == 4 then title.active = "settings" title.menu.maxoptions = 6 end
			if title.menu.selected == 5 then debugstarfield = not debugstarfield end
			if title.menu.selected == 6 then --print ("unimplemented")
			title.active = "credits"
			 end
		elseif title.active == "settings" then
			if title.menu.selected == 1 then title.active = "video" title.menu.maxoptions = 1 end
			if title.menu.selected == 2 then title.active = "sound" title.menu.maxoptions = 1 end
			if title.menu.selected == 3 then title.active = "controls" title.menu.maxoptions = 1 end
			if title.menu.selected == 4 then title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 5 then title.active = "languages" title.menu.maxoptions = 12 end
			if title.menu.selected == 6 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "languages" then
			if title.menu.selected == 1 then title.active = "languages" game.language="da" TransDanish() title.menu.maxoptions = 12 end
			if title.menu.selected == 2 then title.active = "languages" game.language="du" TransDutch() title.menu.maxoptions = 12 end
			if title.menu.selected == 3 then title.active = "languages" game.language="en" TransEnglish() title.menu.maxoptions = 12 end
			if title.menu.selected == 4 then title.active = "languages" game.language="fi" TransFinnish() title.menu.maxoptions = 12 end
			if title.menu.selected == 5 then title.active = "languages" game.language="fr" TransFrench() title.menu.maxoptions = 12 end
			if title.menu.selected == 6 then title.active = "languages" game.language="de" TransGerman() title.menu.maxoptions = 12 end
			if title.menu.selected == 7 then title.active = "languages" game.language="ma" TransMandarin() title.menu.maxoptions = 12 end
			if title.menu.selected == 8 then title.active = "languages" game.language="no" TransNorwegian() title.menu.maxoptions = 12 end
			if title.menu.selected == 9 then title.active = "languages" game.language="po" TransPortuguese() title.menu.maxoptions = 12 end
			if title.menu.selected == 10 then title.active = "languages" game.language="es" TransSpanish() title.menu.maxoptions = 12 end
			if title.menu.selected == 11 then title.active = "languages" game.language="sw" TransSwedish() title.menu.maxoptions = 12 end
			if title.menu.selected == 12 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "video" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4  end
		elseif title.active == "sound" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4 end
		elseif title.active == "controls" then
			if title.menu.selected == 1 then title.active = "settings" title.menu.maxoptions = 4 end
		elseif title.active == "network" then
			if title.menu.selected == 1 then title.active = "enable network" enableRemoteControls=true  title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 2 then title.active = "disable network" enableRemoteControls=false  title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 3 then title.active = "set up network" loadNetworkMenus() enableRemoteControls=true  menuOpened=true NetworkMenuVisible=true title.active = "network" title.menu.maxoptions = 4 end
			if title.menu.selected == 4 then title.active = "settings" title.menu.maxoptions = 6 end
		elseif title.active == "ship_selection" then
			if title.menu.selected == 1 then initarcade(3) end
			if title.menu.selected == 2 then initarcade(1) end
			if title.menu.selected == 3 then initarcade(2) end
			if title.menu.selected == 4 then title.active = "main" title.menu.maxoptions = 7 end
		elseif title.active == "credits" then
			if title.menu.selected == 1 then title.active = "main" title.menu.maxoptions = 7 end
		end
				
		title.menu.selected = 1
		button2or6Pressed=false
		love.timer.sleep(0.1)

			
	if title.menu.selected < 1 then title.menu.selected = 1 end
	if title.menu.selected > title.menu.maxoptions then title.menu.selected = title.menu.maxoptions end
		
end
