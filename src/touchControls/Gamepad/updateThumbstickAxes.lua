scaleFactor=1
function touchControls:updateLeftThumbstickAxes(x, y)
    -- Calculate the relative positions
    local relativeX = x - self.leftThumbstickX
    local relativeY = y - self.leftThumbstickY
    local length = math.sqrt(relativeX^2 + relativeY^2)

    --[[-- Debug prints
    print("Touch Coordinates: ", x, y)
    print("Thumbstick Center: ", self.leftThumbstickX, self.leftThumbstickY)
    print("Relative Position: ", relativeX, relativeY)
    print("Distance from Center: ", length)
--]]
    if length <= self.leftThumbstickRadius / 2 then
        --self.leftxaxis = relativeX / self.leftThumbstickRadius
        --self.leftyaxis = relativeY / self.leftThumbstickRadius
        -- Debug prints
        --print("Inside Thumbstick Radius / 4")
        --print("Left Axis (x, y): ", self.leftxaxis, self.leftyaxis)
    else
    local scaleFactor = self.leftThumbstickRadius / length
			self.leftxaxis = relativeX * scaleFactor
			self.leftyaxis = relativeY * scaleFactor
        --self.leftxaxis = relativeX * scaleFactor
        --self.leftyaxis = relativeY * scaleFactor
        -- Debug prints
        --print("Outside Thumbstick Radius / 4")
        --print("Left Axis (x, y): ", self.leftxaxis, self.leftyaxis)
    end
end
