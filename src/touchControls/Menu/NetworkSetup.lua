	NetworkMenuVisible=false
	
	-- Load LuaSocket library
socket = require("socket")

-- Common base port number
--local basePort = 1024
	receivePort=1024
	sendPort=1025


-- Set the initial instance number
instanceNumber = 1
	
	-- Values for IP range
	ipRangeStart = "192.168.178.0"
	ipRangeEnd = "192.168.178.40"
	
	-- Create a UDP socket
	udp = socket.udp4()
	udp:settimeout(0)



	-- Set the destination IP address and port for sending
	sendAddress = "127.0.0.1"
	--sendAddress = "192.168.178.36"

	-- Set the local IP address and port for receiving
	--receiveAddress = "127.0.0.1"
	--receiveAddress, receivePort = udp:getsockname()
	
		-- Function to execute a system command and capture its output
function executeCommand(command)
    local protectedFunc = socket.protect(io.popen)
    local file, err = protectedFunc(command, 'r')
    if not file then
        print("Error executing command:", err)
        return nil
    end
    local output = file:read('*a')
    file:close()
    return output
end
	
	--receiveAddress = "192.168.178.36"
	
	local result = executeCommand("ifconfig | grep 'inet ' | grep -v '127.0.0.1' | head -n1 | awk '{print $2}'")
	if not result==nil then
		receiveAddress = result:match("%S+")
	else
		receiveAddress="127.0.0.1"
	end
	
	print(receiveAddress)
	--print(receivePort)
	
	udp:setsockname(receiveAddress, receivePort)
	
	EnableRemoteButton = {
	text = "Enable remote control",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+500,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.huge,
    unlocked = true,
	}
	
	NetworkSetupButton = {
	text = "Network setup",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+400,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.huge,
    unlocked = true,
	}
	
	autobindButton = {
	text = "autobind",
	textPosition ="top",
	x = love.graphics.getWidth() / 3, y = love.graphics.getHeight()/1.8,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.huge,
    unlocked = true,
	}
	
		CloseNetworkButton = {
	text = "close network",
	textPosition ="top",
	x = love.graphics.getWidth() /1.3, y = 6,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.huge,
    unlocked = true,
	}
	
	function drawNetworkSetup()
	
		-- Network Setup
	hovered = isButtonHovered (NetworkSetupButton)
	drawButton (NetworkSetupButton, hovered,NetworkSetupButton.text)

	if hovered and love.mouse.isDown(1) then 
		--print(NetworkMenuVisible)
		--print(menuOpened)
		--print(showAbout)
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
				if NetworkMenuVisible==true then 
					NetworkMenuVisible=false
					menuOpened=true showAbout=false
			elseif NetworkMenuVisible==false then 
				NetworkMenuVisible=true
				--print("test")
				require ("touchControls/multipleFields/main")
				
				--require ("touchControls/keyboard/keyboardDraw")
					touchControls.layout = "keyboard"
					touchControls.buttons = {}
					touchControls:init()
					menuOpened=true
					NetworkMenuVisible=true
					showAbout=false
					love.timer.sleep( 0.3 )
				
				--menuOpened=false showAbout=false
			end
			love.timer.sleep( 0.3 )
		end
	elseif hovered then
				love.graphics.print("Network setup", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	
	--touchKeyboard:draw()
		
end


function loadNetworkMenus()
	require ("touchControls/Menu/NetworkSetup")
	require ("touchControls/Network/receiveThumbstickAxes")
	require ("touchControls/Network/sendIdentifier")
 
-- Define a coroutine for listening to messages
	messageCoroutine = {}
	
	require ("touchControls/Network/autobind")
	messageCoroutine = coroutine.create(listenForMessages)	
	require ("touchControls/Network/receiveThumbstickAxes")
	require ("touchControls/Network/sendAxes")
	require ("touchControls/Network/sendIdentifier")
	
	print("Network menus loaded")

end

function EnableRemoteControls()
		-- Enable remote controls
	hovered = isButtonHovered (EnableRemoteButton)
	drawButton (EnableRemoteButton, hovered,EnableRemoteButton.text)

		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
		print(enableRemoteControls)
				if enableRemoteControls==true then 
					enableRemoteControls=false
			elseif enableRemoteControls==false then 
					enableRemoteControls=true
			
				loadNetworkMenus()
			end
			love.timer.sleep( 0.3 )
		
	elseif hovered then
				if enableRemoteControls==true then 
					love.graphics.print("Enable remote controls: Enabled", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
			elseif enableRemoteControls==false then 
				love.graphics.print("Enable remote controls: Disabled", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
			end
	end
end

function drawCloseNetworkSetup()
--Close Network Button
	hovered = isButtonHovered (CloseNetworkButton)
	drawButton (CloseNetworkButton, hovered,CloseNetworkButton.text)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			
			NetworkMenuVisible=false
			 touchControls.layout = "arcade"
			 touchControls:init()
			 menuOpened=false
			 
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("Close Network setup", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end


end

function drawAutobindButton()	
	--Autobind Button
	hovered = isButtonHovered (autobindButton)
	drawButton (autobindButton, hovered,autobindButton.text)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			searchingForInstances=true
			love.graphics.print("Autobind pressed, please wait, searching in process", love.graphics.getWidth() /2,love.graphics.getHeight()/2, 0,1)
			--searchingForValidInstances:play()
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("Auto bind other instances within the range provided", 50,love.graphics.getHeight()-120, 0,1)
			love.graphics.print("This feature does not work yet and is here", 90,love.graphics.getHeight()-90, 0,1)
			love.graphics.print("for further testing and development purposes", 90,love.graphics.getHeight()-60, 0,1)
	end
	
end
