
fonts = {
	default = love.graphics.newFont("data/fonts/0xa000/0xA000-Squareish-Mono.ttf",12),
	timer = love.graphics.newFont("data/fonts/0xa000/0xA000-Squareish-Mono-Bold.ttf",20),
	hud = love.graphics.newFont("data/fonts/robotech-gp/ROBOTECH GP.ttf",20),
	hud_warning = love.graphics.newFont("data/fonts/robotech-gp/ROBOTECH GP.ttf",40),
	paused_large = love.graphics.newFont("data/fonts/0xa000/0xA000-Squareish-Mono-Bold.ttf",40),
	paused_small = love.graphics.newFont("data/fonts/0xa000/0xA000-Squareish-Mono-Bold.ttf",14),
	title_large = love.graphics.newFont("data/fonts/0xa000/0xA000-Squareish-Mono-Bold.ttf",65),
	title_select = love.graphics.newFont("data/fonts/robotech-gp/ROBOTECH GP.ttf",30),
	labels = love.graphics.newFont("data/fonts/robotech-gp/ROBOTECH GP.ttf",18),
	message_header = love.graphics.newFont("data/fonts/robotech-gp/ROBOTECH GP.ttf",23),
	message_content = love.graphics.newFont("data/fonts/0xa000/0xA000-Squareish-Mono.ttf",17),
}

	OpenMenuButton = {
	text = "Menu",
	textPosition ="top",
	x = love.graphics.getWidth() /2, y = 50,
	--x = 100, y = 100,
    sx = 1,
    --image = GamepadLayoutImg,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.title_large ,
    unlocked = true,
	}
	
	QuickSwitchLayoutButton = {
	text = "QuickSwitch",
	textPosition ="top",
	x = love.graphics.getWidth() /1.8, y = 5,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.title_large ,
    unlocked = true,
	}
	
	GamepadLayoutButton = {
	text = "Gamepad layout",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.title_large ,
    unlocked = true,
	}
	
	ArcadeLayoutButton = {
	text = "Arcade layout",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+100,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.title_large ,
    unlocked = true,
	}
	
	KeyboardLayoutButton = {
	text = "Keyboard layout",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+200,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.title_large ,
    unlocked = true,
	}
	
	AboutButton = {
	text = "About",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+700,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.title_large ,
    unlocked = true,
	}
	
	CloseAboutButton = {
	text = "close About",
	textPosition ="top",
	x = love.graphics.getWidth() /1.3, y = 6,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.title_large ,
    unlocked = true,
	}
	
	SwitchPortraitLanscapeButton = {
	text = "Portrait - Landscape",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = 50,  y = love.graphics.getHeight() / 4+300,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = fonts.title_large ,
    unlocked = true,
	}

menuOpened=false
showAbout=false

function drawOpenMenu()
	if showAbout==false then
-- Open menu button
	hovered = isButtonHovered (OpenMenuButton)
	drawButton (OpenMenuButton, hovered,OpenMenuButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		showAbout=false
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) or touchControls:checkButtonPress(x, y) then
				 if menuOpened==true then menuOpened=false
				 love.timer.sleep( 0.3 )
				 return
			 elseif menuOpened==false then menuOpened=true
			love.timer.sleep( 0.3 )
			 return
			
			end
		end
	elseif hovered then
				love.graphics.print("Settings menu", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	
	--[[ disabled
	-- Quick Switch button
	hovered = isButtonHovered (QuickSwitchLayoutButton)
	drawButton (QuickSwitchLayoutButton, hovered,QuickSwitchLayoutButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			 touchControls:toggleLayout()
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("Quick switch layout", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	--]]
	end
	
end

function drawMenuButtons()


if menuOpened==true and showAbout==false then

	--gamepad Layout Button
	hovered = isButtonHovered (GamepadLayoutButton)
	drawButton (GamepadLayoutButton, hovered,GamepadLayoutButton.text)

	if hovered and (love.mouse.isDown(1) or touchControls:checkButtonPress(x, y)) then 
	--inputtime=0
		
			  touchControls.layout = "gamepad"
			 touchControls:init()
			 menuOpened=false
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		
	elseif hovered then
				love.graphics.print("Gamepad layout", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end

--Arcade Layout Button
	hovered = isButtonHovered (ArcadeLayoutButton)
	drawButton (ArcadeLayoutButton, hovered,ArcadeLayoutButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			  touchControls.layout = "arcade"
			 touchControls:init()
			 menuOpened=false
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
				love.graphics.print("Arcade layout", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	
	--Keyboard Layout Button
	hovered = isButtonHovered (KeyboardLayoutButton)
	drawButton (KeyboardLayoutButton, hovered,KeyboardLayoutButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			  touchControls.layout = "keyboard"
			 touchControls.buttons = {}
			 touchControls:init()
			 menuOpened=false
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
				love.graphics.print("Keyboard layout", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
--[[	
	--Switch portrait / Landscape mode Button
	hovered = isButtonHovered (SwitchPortraitLanscapeButton)
	drawButton (SwitchPortraitLanscapeButton, hovered,SwitchPortraitLanscapeButton.text)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
				if KeyboardOrientation == "landscape" then
					KeyboardOrientation = "portrait"
					love.window.setMode(768, 1024)
			elseif KeyboardOrientation == "portrait" then
					KeyboardOrientation = "landscape"
					love.window.setMode(1080, 2340)
			end
			--selectKeyboardLayout()
			--
			touchControls:init()
			 menuOpened=false
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
				love.graphics.print("This option rotates the keyboard", love.graphics.getWidth() /3,love.graphics.getHeight()-100, 0,1)
				love.graphics.print("This is useful if your LÖVE build does not feature device orientation", 100,love.graphics.getHeight()-50, 0,1)
	end
	--]]
--About Button
	hovered = isButtonHovered (AboutButton)
	drawButton (AboutButton, hovered,AboutButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			showAbout=true
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("About", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	
end
if showAbout==true then
--Close About Button
	hovered = isButtonHovered (CloseAboutButton)
	drawButton (CloseAboutButton, hovered,CloseAboutButton.text)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("Close about", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end

end


end

function renderCloseAbout()
hovered = isButtonHovered (CloseAboutButton)
	drawButton (CloseAboutButton, hovered,CloseAboutButton.text)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("Close about", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end

end

