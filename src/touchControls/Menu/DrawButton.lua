--[[This code defines a function drawButton that takes three parameters: button, hovered, and text.
This function is used to draw a button on the screen with the specified properties.--]]

function drawButton (button, hovered,text)	--Defines a function drawButton with three parameters: button, hovered, and text.
	if not button.font==nil then
		love.graphics.setFont( button.font )	--Sets the font of the button to the font specified in button.font.
	else
		love.graphics.setFont( fonts.title_large  )	--Sets the font of the button to the font specified in button.font.
	end
	-- regular buttons (without image)
    if hovered then									--If the button is being hovered over, 
		love.graphics.setColor(button.hoveredColor)	--the button color is set to button.hoveredColor, 
		
		 -- Draw a rectangle around the button when hovered
        love.graphics.rectangle('line', button.x, button.y, button.w, button.h)
    else
        love.graphics.setColor(button.color)
    end    
	

	    
    -- If the button is an image
     if button.image then
        local x, y, width, height = button.x, button.y, button.image:getWidth(), button.image:getHeight()
		if hovered then									--If the button is being hovered over, 
			love.graphics.setColor(1,0.5,0.5,1)
		end
        -- Calculate the position for the text based on textPosition
        if textPosition == "top" then
            y = y - height / 2  -- Adjust the y-coordinate to be at the top of the image button
        elseif textPosition == "bottom" then
            y = y + height / 2  -- Adjust the y-coordinate to be at the bottom of the image button
        end

        -- Draw the image button
        love.graphics.draw(button.image, x, y, button.r, button.sx)
    else
        -- Handle regular buttons without images here
        love.graphics.print(button.text, button.x, button.y, button.r, button.sx)
    end

	love.graphics.print(button.text,button.x,button.y,button.r,button.sx)	--Prints the specified text on the button at the position specified by button.x and button.y with the specified rotation button.r and scale button.sx.
	
	
end
