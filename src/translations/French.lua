

function TransFrench()

trans.InfiniteMode = "Mode Infini"
trans.DebugMode = "Mode Débogage"
trans.Settings = "Paramètres"
trans.SpaceViewer = "Visionneuse d'Espace"
trans.Credits = "Crédits"
trans.ExitToDesktop = "Quitter et retourner au bureau"
trans.Video = "Vidéo"
trans.Sound = "Son"
trans.Controls = "Contrôles"
trans.Back = "Retour"
trans.Languages = "Langues"
trans.Danish="Danois"
trans.Dutch="Néerlandais"
trans.English="Anglais"
trans.Finnish="Finnois"
trans.French="Français"
trans.German="Allemand"
trans.Mandarin="Mandarin"
trans.Norwegian="Norvégien"
trans.Portuguese="Portugais"
trans.Spanish="Espagnol"
trans.Swedish="Suédois"

trans.ShipSelection = "Sélection du vaisseau"
trans.Ship1 = "Vaisseau 1"
trans.Ship2 = "Vaisseau 2"
trans.Ship3 = "Vaisseau 3"


trans.text1 = "Ici le Capitaine Zara Khan pour le Pilote Johnson. Veuillez répondre."
trans.text2 = "Johnson ici, Commandant ! Quelle est la situation ?"
trans.text3 = "Nous avons détecté une signature énergétique étrange venant du système planétaire proche."
trans.text4 = "Reçu. Le temps estimé jusqu'à la surface de la planète est... 3 minutes."
trans.text5 = "Commence la descente maintenant."
trans.text6 = "Négatif, Pilote. Nous venons de capter un autre signal - c'est un vaisseau alien se dirigeant droit vers la planète !"
trans.text7 = "Quoi ?! Ça change les choses. Pouvez-vous me donner des informations sur le vaisseau ennemi ?"
trans.text8 = "C'est un croiseur lourd, fortement armé. Ils se rapprochent rapidement. Vous devez agir rapidement."
trans.text9 = "Je m'en occuperai. Fin de la transmission."
trans.text10 = "Bonne chance, Pilote. J'espère que vous savez ce que vous faites."

end
