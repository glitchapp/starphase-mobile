-- Nebula shader to simulate a space-like nebula with color variation and turbulence
nebulaShader = love.graphics.newShader([[
extern float time;           // Animation time
extern vec2 parallaxOffset;  // Parallax scrolling offset
extern vec2 resolution;      // Screen resolution

// New uniforms for nebula color and alpha
extern vec3 nebulaColor;     // RGB color for the nebula
extern vec3 backgroundColor; // RGB color for the background space
extern float alpha;          // Overall alpha transparency

// Simplex/Perlin noise function (pseudo-random)
float hash(vec2 p) {
    return fract(sin(dot(p, vec2(127.1, 311.7))) * 43758.5453123);
}

float noise(vec2 p) {
    vec2 i = floor(p);
    vec2 f = fract(p);

    vec2 u = f * f * (3.0 - 2.0 * f);

    return mix(mix(hash(i + vec2(0.0, 0.0)), 
                   hash(i + vec2(1.0, 0.0)), u.x),
               mix(hash(i + vec2(0.0, 1.0)), 
                   hash(i + vec2(1.0, 1.0)), u.x), u.y);
}

// Fractal Brownian Motion (FBM) for nebula texture with dynamic movement
float fbm(vec2 p, float time) {
    float value = 0.0;
    float amplitude = 0.5;
    float frequency = 1.0;
    for (int i = 0; i < 6; i++) {  // Reduced iterations for smoother texture
        value += amplitude * noise(p * frequency);
        frequency *= 2.0 + sin(time * 0.1) * 0.3; // Oscillating frequency for motion
        amplitude *= 0.5 + cos(time * 0.1) * 0.1; // Oscillating amplitude for depth
    }
    return value;
}

// Effect function to generate the nebula effect
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
    // Normalize coordinates based on resolution
    vec2 uv = screen_coords / resolution + parallaxOffset;

    // Scale UV coordinates for the nebula details
    vec2 p = uv * 4.0 - vec2(2.0); // Adjust scale for finer detail
    p += vec2(sin(time * 0.03) * 0.3, cos(time * 0.05) * 0.2); // Turbulent motion

    // Generate nebula pattern using FBM with animated parameters
    float nebula = fbm(p, time);

    // Soft threshold for nebula opacity (density control)
    float opacity = smoothstep(0.2, 0.7, nebula);

    // Dynamic nebula color based on time (for animation)
    vec3 dynamicNebulaColor = nebulaColor * (0.5 + 0.5 * sin(time * 0.2)); // Oscillate color for variation

    // Blend between background space color and nebula color based on opacity
    vec3 finalColor = mix(backgroundColor, dynamicNebulaColor, opacity);

    // Apply overall alpha transparency
    return vec4(finalColor, alpha) * color; // Apply vertex color if needed
}
]])

