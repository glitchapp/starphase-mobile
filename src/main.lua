--[[
 * Copyright (C) 2016-2019 Ricky K. Thomson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * u should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 --]]

function love.load(args)

 	require("NoobHub/main")	-- load NoobHub Client
require("chatHub/chatHub")	-- chat hud based on noobHUB
noobHubEnabled = false
chathudFlag = false

	touchEnabled = false
	--require("loadAssets")
	--WebP = require("love-webp")			-- webp library for love

	require("hud")
	require("loading")

	require("misc")
	require("camera")
	require("binds")
	require("joystick")
	require("collision")
	require("sound")
	require("textures")
	require("title")
	
	require("fonts")
	require("entities/pickups")
	require("entities/enemies")
	require("entities/explosions")
	require("entities/player")
	require("entities/player2")
	require("entities/projectiles")
	require("starfield")
	
	require("SomeDialogs")
	require("translations")
	require("cutscenes")
	
	
	require("nebula")
	
	

	msgs = require("messagebox")
	
	msgs.callback = function() print("end of message test") end

	debug = false
	debugarcade = false
	
	cheats = {
		invincible = false,
	}
	
	
--require ("ErrorHandling")

	require ("getOs")
	--currentOS = getOS()					-- does not work when installed from rpm builds
	currentOS = love.system.getOS( )	-- can not distinguish from linux desktop and linux mobile systems
	print("Current OS: " .. currentOS)
	
	if currentOS=="Linux" then rotationEnabled=true
	else rotationEnabled=false
	end
 
	LastOrientation="portrait"
	MobileOrientation="portrait"
	
--love.window.setMode(1080, 2340, {resizable=true, borderless=false})
 
 

local success, error_message = pcall(function()
    joystick = love.joystick.getJoysticks()[1]
    require("gamepadinput")
    loadcontrollermappings()
    gamepadTimer = 0
end)
	

enableRemoteControls=false	-- set it to true if you want to use remote controls by default
		if enableRemoteControls==true then
			 print("Remote controls enabled")
			  loadNetworkMenus()
		end
		require ("touchControls/touchKeyboard")
		KeyboardOrientation="portrait"
		 touchControls = require("touchControls/touchControls")
		 touchControls:init()
		 

	--parse command line arguments to the game
	for _,arg in pairs(args) do
		if arg == "-debug" or arg == "-d" then debug = true end
		if arg == "-fullscreen" or arg == "-f" then love.window.setFullscreen(1) end
		if arg == "-mute" or arg == "-m" then sound.enabled = false end
	end
	
	game = {}
	
	game.width, game.height, game.flags = love.window.getMode( )
	game.seed = nil
	game.max_fps = game.flags.refreshrate
	--game.max_fps = 60
	game.min_dt = 1/game.max_fps
	game.next_time = love.timer.getTime()

	cursor = love.mouse.newCursor( "data/gfx/cursor.png", 0, 0 )
	love.mouse.setCursor(cursor)	

	sound:init()
	title:init()
	
	timer=0
	
	TransEnglish()
	game.language="en"
	
	
	
end

function loadNetworkMenus()
	--require ("touchControls/Menu/NetworkSetup")
	--require ("touchControls/Network/receiveThumbstickAxes")
	--require ("touchControls/Network/sendIdentifier")
	--require ("touchControls/multipleFields/main")
	
-- Define a coroutine for listening to messages
	messageCoroutine = {}
	
	--require ("touchControls/Network/autobind")
	--messageCoroutine = coroutine.create(listenForMessages)	
	--require ("touchControls/Network/sendAxes")
	
	
	--print("Network menus loaded")

end


--test function
function initarcade(playersel)
	-- arcade mode will use preset seeds determined on each level
	-- whereas infinite mode will use complete random eventually.
	starfield:setSeed()
	starfield:populate()
	starfield.minspeed = 30
	starfield.maxspeed = 1000
	
	love.mouse.setVisible(true)
	love.mouse.setGrabbed(false)
	paused = false
	debugarcade = false
	mode = "arcade"
	love.graphics.setBackgroundColor(0,0,0,255)
		
	enemies.spawned = 0

	player:init(playersel)
	if twoplayers==true then
		player2:init(playersel)
	end
	
	hud:init()

	local s = love.math.random(3,#sound.music)
	sound:playbgm(s)
	

end
MessageNumber=1

function initdebugarcade(playersel)
	initarcade(playersel)
	debugarcade = true
	debug = true
	msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/1.png"),
				name = "Debug Mode",
				text = "Testing messagebox events...\n\nPress [M] to start dialog test\nPress [SPACE] to skip messages",
				duration = 60,
			}

	})
end


--leftxaxis,leftyaxis=0,0
function love.update(dt)

 if noobHubEnabled == true then
		NoobHubClientUpdate(dt)			-- online multiplayer based on NoobHub (https://github.com/Overtorment/NoobHub)
	end
	
	if chathudFlag == true then
		chathudUpdate(dt)	-- this update a function that check gptCoroutine, a coroutine that works on a second thread to keep the game running 
	end	
	
	updategetjoystickaxis(dt)
	
	if mode == "title" then
		title:GamePadpressed(joystick,button)
	end
	
	  if enableRemoteControls==true then
	  
			receiveThumbstickAxes()	-- remote network controls
			if identifierSent==nil then
				print("sending identifier to remote control... no response")
				sendIdentifier()
			end
			
	 end
	
	--cap fps

	--use this to slow down when setting < 60fps
	--dt = math.min(dt, game.min_dt)
		
	game.next_time = game.next_time + game.min_dt

	--process arcade game mode
	if mode == "arcade" then
		
		starfield:update(dt)
		
		--if MessageNumber>10 then -- the intro dialogs finished
			projectiles:update(dt)
			enemies:update(dt)
			explosions:update(dt)
			pickups:update(dt)
			
		--end
	
		player:update(dt)
		if twoplayers==true then
			player2:update(dt)
		end
		hud:update(dt)
		msgs.update(dt)
		PlaySomeDialogs(dt)
	
		if ShieldLowSceneLoaded and not ShieldLowScenePlayed and ShieldLowScenePlaying and hud.display.progress > 725 then
            updateShieldLowCutscene(dt)
        end
	end
		
		
	--process titlescreen
	if mode == "title" then
		title:update(dt)
	end
	--if menuOpened==true then
		
	--end
	if NetworkMenuVisible== true then
	if love.mouse.isDown(1) then
		updateKeyboard(dt)
	end

		loveInputFieldUpdate(dt)
		
	end
	
		-- Cutscene after beating the boss
	if Boss1Destroyed == true and cutscene1Loaded == false then
		loadCutscene1()
	end
	-- Update cutscene while is playing
	if Cutscene1Played == false and cutscene1Loaded == true and Boss1Destroyed == true then
		updateCutscene1(dt)
	end
	
	--credits after playing the cutscenes
	if Cutscene1Played == true and cutscene1Loaded == true and Boss1Destroyed == true then
		updateCredits(dt)
	end
		
	--process the debug console
	hud:updateconsole(dt)
	if rotationEnabled == true then
		--checkOrientation()
	end
	
	
end

function love.draw()

	
	if MobileOrientation == "portrait" then
		--touchControls:draw()
		--love.graphics.scale( 0.9 )
		--love.graphics.rotate( -1.57 )
		--love.graphics.translate(100,100)
		
	elseif MobileOrientation == "landscape" then
		--love.graphics.rotate( 1.57 )
		--love.graphics.scale( 0.7,0.7 )
		--love.graphics.rotate( 1.57 )
		--love.graphics.translate(130,-1350)
		--touchControls:draw()
	end
	
	
	

if  menuOpened==false then

	--draw title screen
if mode == "title" then
		title:draw()
	end

	if mode == "arcade" and Cutscene1Playing==false then
		--starfield:draw(0,-player.y/4)
		if ShieldLowScenePlaying==false then
			starfield:draw(0,0)
			hud:draw()
			msgs.draw()
		
		--debug
		--print(ShieldLowSceneLoaded)
		--print(ShieldLowScenePlayed)
		--print(ShieldLowScenePlaying)
		end
		 if ShieldLowSceneLoaded and not ShieldLowScenePlayed and ShieldLowScenePlaying and hud.display.progress > 725 then
            drawShieldLowCutscene()
        end
	end
	
	-- Cutscene after beating the boss
	if cutscene1Loaded==true and Cutscene1Played==false and Boss1Destroyed==true then
		playCutscene1()
	end
	
	--Draw the credits after playing the cutscenes
	if endCredits == true then
		creditsdraw(dt)
	end

	--draw the debug console
	hud:drawconsole()

	-- caps fps
	local cur_time = love.timer.getTime()
	if game.next_time <= cur_time then
		game.next_time = cur_time
		return
	end
	love.timer.sleep(game.next_time - cur_time)
	
	
end
	if mode == "title" then
		--drawNetworkMenu()
	end
	
	if chathudFlag == true then
		chathuddraw()
	end
	
end


--require ("touchControls/Menu/Buttons")

function drawNetworkMenu()

	 if showAbout==true then
		if not (love.graphics.getFont()==fonts.huge) then
			--love.graphics.setFont(fonts.huge)
		end
	love.graphics.print("About: ",10, 10,0,1)
		drawTextContent(paragraph, windowX, windowY, maxWidth, lineHeight)
		renderCloseAbout()
	end


if menuOpened==false then
		if not (touchControls.leftThumbstickX==nil) then
			if touchEnabled == true then
				touchControls:draw()
			end
		end
  
	--if touchControls.layout=="keyboard" then
		--touchKeyboard:draw()
	--end

	-- debug statements
	-- Print values on the game window
	--love.graphics.print("Left Thumbstick - x: " .. touchControls.leftxaxis .. " y: " .. touchControls.leftyaxis, 10, 10)
	--love.graphics.print("Right Thumbstick - x: " .. touchControls.rightxaxis .. " y: " .. touchControls.rightyaxis, 10, 30)
		
		if mode == "title" then drawOpenMenu() end
elseif menuOpened==true and not (showAbout==true) then
		
		drawOpenMenu()
		drawMenuButtons()
		drawNetworkSetup()
		EnableRemoteControls()
			if NetworkMenuVisible==false then
				drawMenuButtons()
			elseif NetworkMenuVisible==true then
				loveInputFieldDraw()
				--drawAutobindButton()
				drawCloseNetworkSetup()
			end
			
			if touchControls.layout=="keyboard" then
				touchKeyboard:draw()
			end
	end
	 
end

function love.keypressed(key)

	if debugarcade then 
		if key == "k" then 
			pickups:add(starfield.w/2,starfield.h/2) 
		end 
	end

	--global controls
	if key == binds.fullscreen then misc:togglefullscreen() end
	if key == binds.console then debug = not debug end
	
	--arcade mode controls
	if mode == "arcade" then
		if key == binds.pause then misc:togglepaused() end
	
		if paused then
			if key == binds.pausequit then  title:init() end
		end
	end
	
	--title screen controls
	if mode == "title" then
		title:keypressed(key)
	end
	
	msgs:keypressed(key)
	
	--debug enemy
	if debugarcade then
	if key == "1" then
		enemies:add_abomination()
	elseif key == "2" then
		enemies:add_asteroid()
	elseif key == "3" then
		enemies:add_crescent()
	elseif key == "4" then
		enemies:add_dart()
	elseif key == "5" then
		enemies:add_delta()
	elseif key == "6" then
		enemies:add_large()
	elseif key == "7" then
		enemies:add_train()
	elseif key == "8" then
		enemies:add_tri()
	elseif key == "9" then
		enemies:add_cruiser()
	elseif key == "tab" then
		
		starfield:setSeed()
		--starfield:populate()
		
		msgs.queue({
			{		
				face = love.graphics.newImage("data/gfx/faces/1.png"),
				name = "DEBUG: new seed",
				text = love.math.getRandomSeed(),
				duration = 1,
			}
		})
	
	elseif key == "m" then
		msgs.queue(
		{
			{		
				face = love.graphics.newImage("data/gfx/faces/face" .. love.math.random(1,10)..".png"),
				name = "Message Test",
				text = "Hello",
				duration = 2,
			},
			{		
				face = love.graphics.newImage("data/gfx/faces/face" .. love.math.random(1,10)..".png"),
				name = "Message Test",
				text = "I want to stop flying this ship!",
				duration = 3,
			},
			{		
				face = love.graphics.newImage("data/gfx/faces/face" .. love.math.random(1,10)..".png"),
				name = "Message Test",
				text = "PEW PEW PEW\n\n... *TRANSMISSION ERROR*",
				duration = 2,
			},
			{		
				face = love.graphics.newImage("data/gfx/faces/face" .. love.math.random(1,10)..".png"),
				name = "Message Test",
				text = "Trololololol lololol lololol...\nLolol lololololol",
				duration = 3,
			},
			{		
				face = love.graphics.newImage("data/gfx/faces/face" .. love.math.random(1,10)..".png"),
				name = "Message Test",
				text = "Random gibberish, that doesn't make sense.\nJust to pretend this is a real dialog\nI don't even know...",
				duration = 4,
			}
		})
		--[[
		msgs.queue(
		{
			
		})
		--]]
	end
	
	
	end
	--[[
	if key == "0" then
		love.window.setMode(1024,(game.scale.h/game.scale.w)*1024 )
	end
	
	if key == "9" then
		love.window.setMode(800,(game.scale.h/game.scale.w)*800 )
	end
	
	if key == "8" then
		love.window.setMode(1200,(game.scale.h/game.scale.w)*1200 )
	end
	
	--]]
	
	--love.window.setMode(1200,(game.scale.h/game.scale.w)*1200 )

	sound:keypressed(key)
end

--function love.mousepressed(x,y,button) 

--end




function love.wheelmoved(x, y)
	if y < 0 then 
		camera.scale = math.max(camera.scale +0.01,0.01)
	elseif y > 0 then
		camera.scale = camera.scale -0.01
	end
end


--[
function love.focus(f)
	if f then
		print("Window grabbed focused.")
	else
		print("Window lost focus.")
		if mode == "arcade" then
			misc:pause()
		end
	end
end


function love.resize(w,h)
	game.width = w
	game.height = h
end

function checkOrientation()
	local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
	
	--MobileOrientation="landscape"
		if desktopWidth<1100 then MobileOrientation="portrait"
			if LastOrientation==MobileOrientation then 

			else	
				LastOrientation=MobileOrientation
				
				love.window.setMode(2340, 1080, {resizable=true, borderless=false})
				touchControls:createArcadeButtons()
			end
	elseif desktopWidth>1100  then MobileOrientation="landscape"
			if LastOrientation==MobileOrientation then 

			else	
				LastOrientation=MobileOrientation
				
				love.window.setMode(1080, 2340, {resizable=true, borderless=false})
				touchControls:createArcadeButtons()
			end
	end
end

