# Star Phase (Fork from https://github.com/Jigoku/starphase)

This source is tweaked for mobile phones, if you want to play this game on a desktop with all the features and music tracks I suggest downloading the desktop source from: https://codeberg.org/glitchapp/Starphase

<img src="screenshots/sc1.avif" width=70% height=70%>
<img src="screenshots/sc2.avif" width=70% height=70%>
<img src="screenshots/sc3.avif" width=70% height=70%>
I'm not the original developer of this game, the original repository can be found on the link above.

I added to this fork:

- Thumbstick control support (You can navigate menus and play the game with the left thumbstick)
- Touch controls (work in progress)
- Love-wepb library (game size reduced from 272Mb to 138,7Mb, 50% less space needed)
- two players mode
- Network remote controls (in progress)


# Star Phase
A top down / side scrolling space shooter made with Lua and Love2D. 

This is an early work in progress, so many bugs / changes will come.

Reccomended to use 1920x1080 resolution for now. ([issue #9](https://github.com/Jigoku/starphase/issues/9))

### Running the game
Run the game from the src/ directory using
```
$ love src/
```

To create standalone executables for all platforms 
```
$ make && make all
```

### Requirements
Install the love2d library from here https://love2d.org

### Controls
See `binds.lua` for game controls

# License
GNU GPLv3 / CC-BY-SA

# Screenshots

![1](screenshots/1.jpg)

![2](screenshots/2.jpg)

![3](screenshots/3.jpg)

![4](screenshots/4.jpg)
