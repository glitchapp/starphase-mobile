Name:       Starphase
# >> macros
%define _binary_payload w2.xzdio
# << macros

%{!?qtc_qmake:%define qtc_qmake %qmake}
%{!?qtc_qmake5:%define qtc_qmake5 %qmake5}
%{!?qtc_make:%define qtc_make make}
%{?qtc_builddir:%define _builddir %qtc_builddir}

Version:    0.1.4b
Release:    1%{?dist}
Summary:    Side scrolling space shooter using LÖVE and Lua (forked from https://github.com/Jigoku/starphase/issues)

License:    GPLv2+
BuildArch:  noarch
URL:        https://codeberg.org/poetaster/starphase-mobile
Source0:    %{name}-%{version}.tar.bz2

BuildRequires: zip 

%description

A top down / side scrolling space shooter made with Lua and Love2D.

This is an early work in progress, so many bugs / changes will come.

Reccomended to use 1920x1080 resolution for now.

 I added to this fork:

    Thumbstick control support (You can navigate menus and play the game with the left thumbstick)
    Added touch controls and several tweaks to run on mobiles (check releases for more info).
    Love-wepb library (game size reduced from 272Mb to 138,7Mb, 50% less space needed)
    Two players mode (work in progress)
    Dialogs
    Boss (first stage)
    Credits

%if "%{?vendor}" == "chum"
PackageName: Starphase
Type: desktop-application
Categories:
 - Game
DeveloperName: glitchapp 
Custom:
 - Repo: https://codeberg.org/glitchapp/Starphase
Icon: https://codeberg.org/glitchapp/starphase-mobile/raw/branch/main/src/starphase/starphase.png
Screenshots:
 - https://codeberg.org/glitchapp/Starphase/media/branch/main/screenshots/1.jpg
 - https://codeberg.org/glitchapp/Starphase/media/branch/main/screenshots/2.jpg
 - https://codeberg.org/glitchapp/Starphase/media/branch/main/screenshots/3.jpg
 - https://codeberg.org/glitchapp/Starphase/media/branch/main/screenshots/4.jpg
 
%endif

%prep
%setup -q -n %{name}-%{version}


%define _sourcedir ../

%build
# >> build pre
# << build pre

zip -q %{_sourcedir}starphase.zip src/*
mv %{_sourcedir}starphase.zip %{_sourcedir}starphase.love


%install
mkdir -p %{buildroot}/usr/share/applications
mkdir -p %{buildroot}/usr/share/games/love/Starphase

install -m 0644 %{_sourcedir}/starphase.love %{buildroot}/usr/share/games/love/Starphase/starphase.love
install -m 0755 starphase.desktop %{buildroot}/usr/share/applications/starphase.desktop
install -m 0644 starphase.png %{buildroot}/usr/share/games/love/Starphase/starphase.png

%files
/usr/share/applications/starphase.desktop
/usr/share/games/love/Starphase/starphase.love
/usr/share/games/love/Starphase/starphase.png

%changelog
